﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFControlServices.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IConfigurationService" in both code and config file together.
    [ServiceContract]
    public interface IConfigurationService
    {
        [OperationContract]
        Dictionary<string, string> GetConfiguration();

        [OperationContract]
        bool IsConfigurationActual(string hash);
    }
}
