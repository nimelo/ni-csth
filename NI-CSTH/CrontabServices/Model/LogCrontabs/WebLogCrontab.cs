﻿using DBModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace CrontabServices.Model
{
    public class WebLogCrontab : CrontabLogBase<WebHistory>
    {
        #region Methods

        protected override List<WebHistory> ExtractLog(string log, History history)
        {
            List<WebHistory> newWebHistory = new List<WebHistory>();
            List<string> domains = new List<string>();

            var splittedLog = log.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (splittedLog.Length % 4 != 0) throw new Exception();

            for (int i = 0; i < splittedLog.Length; i += 4)
            {
                var from = DateTime.Parse(splittedLog[i]);
                var timespan = new TimeSpan(0, 0, int.Parse(splittedLog[i + 2]));
                var to = DateTime.Parse(splittedLog[i + 3]);
                var url = splittedLog[i + 1];
                var domain = new Uri(url).Host;

                var webItemPeriod = new WebItemPeriod()
                {
                    From = from,
                    TimeSpan = timespan,
                    To = to
                };

                var webHistory = new WebHistory()
                {
                    Url = url,
                    Domain = domain,
                    HistoryId = history.Id,
                    WebItemPeriod = webItemPeriod
                };

                if (domains.Contains(domain))
                {
                    webHistory.WebHistoryDomains = newWebHistory.Where(x => x.Domain == domain).First().WebHistoryDomains;
                    newWebHistory.Where(x => x.Domain == domain).First().WebHistoryDomains.WebItemPeriods.Add(webItemPeriod);
                    newWebHistory.Where(x => x.Domain == domain).First().WebHistoryDomains.SumTimeSpan += webItemPeriod.TimeSpan;
                }
                else
                {
                    var webDomain = new WebHistoryDomain()
                    {
                        Domain = domain,
                        WebItemPeriods = new List<WebItemPeriod>() { webItemPeriod},
                        WebHistories = new List<WebHistory>() { webHistory },
                        HistoryId = history.Id,
                        SumTimeSpan = webItemPeriod.TimeSpan
                        
                    };
                    webHistory.WebHistoryDomains = webDomain;

                    domains.Add(domain);
                }

                newWebHistory.Add(webHistory);
            }
            return newWebHistory;
        }

        protected override void SaveToDB(List<WebHistory> newWebHistory, int historyId)
        {
            using (var db = new DbModelContainer())
            {
                db.WebHistorySet.AddRange(newWebHistory);
                db.HistorySet.Where(x => x.Id == historyId).Single().ExistWebHistories = true;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        protected override IEnumerable<string> SeparateApprovedFiles(string[] files)
        {
            return files.Where(f =>
            {
                return this.UserHistories.Where(us => us.ExistWebHistories == false)
                            .Select(s => s.Day.ToShortDateString())
                            .Contains(Path.GetFileName(f).Replace(".log", string.Empty));

            }).ToList();
        }

        #endregion

        public WebLogCrontab(Crontab crontab) : base(crontab)
        {
        }
    }
}
