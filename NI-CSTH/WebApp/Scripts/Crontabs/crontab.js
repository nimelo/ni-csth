﻿

function renderCrontab(crontabName, proc, err, amount, errorFileList) {
    var html = "";
    html += '<div class="clr">';
    html += '<h2 align="center">' + crontabName + '</h2>';

    html += '<div class="' + ((proc < amount) ? warningAlertClass : successAlertClass) + '">';
    html += '<div class="left">Processed:</div>';
    html += '<div class="right">' + proc + "/" + amount + '</div>';
    html += '</div>';

    html += '<div class="' + ((err != 0) ? errorAlertClass : successAlertClass) + '">';
    html += '<div class="left">Errors in files:</div>';
    html += '<div class="right">' + err + "/" + amount + '</div>';

    html += '</div>';
    if (err != 0) {
        html += renderErrorFileTable(errorFileList);
    }
    html += '</div>';
    return html;
}

function renderErrorFileTable(errorFileList) {
    var html = "";
    html += '<table class="table" border="1"><thead><tr><td>Id</td><td>Login</td><td>DateOfFile</td><td>ProcessingDate</td></tr></thead>'
    $.each(errorFileList, function (index, el) {
        html += '<tr>';
        html += '<td>' + (index + 1) + '</td>';
        html += '<td>' + el.Login + '</td>';
        html += '<td>' + el.DateOfFile + '</td>';
        html += '<td>' + el.ProcessingDate + '</td>';
        html += '</tr>';
    });

    html += '</table>';
    return html;
};

var errorAlertClass = 'container alert alert-danger';
var successAlertClass = 'container alert alert-success';
var warningAlertClass = 'container alert alert-warning';

$(function () {

    var exampleFileList = [
    {
        login: "login1",
        Date: 23423,
        ProcessingDate: 123
    }, {
        login: "login2",
        Date: 23423,
        ProcessingDate: 123
    }];


    //$('#crontabContainer').append(renderCrontab("Web crontab", 9, 3, 10, exampleFileList));
    //$('#crontabContainer').append(renderCrontab("App crontab", 10, 0, 10));
    //$('#crontabContainer').append(renderCrontab("History upgrade", 10, 0, 10));
    $('.alert-danger').click(function (e) {
        e.preventDefault();
        // this.append("sdf");
        console.log(this);
        $(this).next().toggle();
        console.log()
    });

    getCrontabStatuses(renderCrontabs);
});

function getCrontabStatuses(callback) {
    $.get("/Crontabs/GetCrontabStatuses", {}, function (data) {
        callback(data)
    }).fail(function () { })
}

function renderCrontabs(data) {
    $.each(data, function (index, el) {
        $('#crontabContainer').append(renderCrontab(el.Name, el.Good, el.Fail, el.Processed, el.Errors))
    })
}

