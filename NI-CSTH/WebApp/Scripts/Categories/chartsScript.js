﻿$(function () {

    var exampleData = [{
        name: "Microsoft Internet Explorer",
        y: 56.33
    }, {
        name: "Chrome",
        y: 24.03,
        sliced: true,
        selected: true
    }, {
        name: "Firefox",
        y: 10.38
    }, {
        name: "Safari",
        y: 4.77
    }, {
        name: "Opera",
        y: 0.91
    }, {
        name: "Proprietary or Undetectable",
        y: 0.2
    }];


    ChartsRendering.renderPie('#firstCategory', exampleData);

    function getRenderableCategories() {
        $.get("/Categories/GetRenderableCategories", {}, function (data) {

            $.each(data, function (index, el) {
                addCategory(el.Name, el.Id);
                getCategoryData(el.Id);
            })

        }).fail(function () {
            alert('fail')
        })
    }

    function getCategoryData(id) {
        $.get("/Categories/GetChartDataForCategory", {categoryId : id, from: "", to: "", login: "", forAll: true}, function (data) {
            ChartsRendering.renderPie('#category' + id, data);
        }).fail(function () {
            alert('fail')
        })
    }

    function addCategory(name, id) {
        html = ""
        html += '<div class="left half">'
        html +='<h2 class="text-center">' + name + '</h2>'
        html += ' <div id="category' + id + '" class="basic-chart"></div></div>'
      
        $('#charts').append(html);
    }

    getRenderableCategories();
});