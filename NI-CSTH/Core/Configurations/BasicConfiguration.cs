﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Configurations
{
    public class BasicConfiguration : ConfigurationSection
    {
        #region ATL
        [ConfigurationProperty("ATLExecutablePath")]
        public string ATLExecutablePath
        {
            get { return (string)this["ATLExecutablePath"]; }
            set { this["ATLExecutablePath"] = value; }
        }

        [ConfigurationProperty("ATLLoggingPath")]
        public string ATLLoggingPath
        {
            get { return (string)this["ATLLoggingPath"]; }
            set { this["ATLLoggingPath"] = value; }
        }
        #endregion

        #region WBTL
        [ConfigurationProperty("WBTLExecutablePath")]
        public string WBTLExecutablePath
        {
            get { return (string)this["WBTLExecutablePath"]; }
            set { this["WBTLExecutablePath"] = value; }
        }

        [ConfigurationProperty("WBTLLoggingPath")]
        public string WBTLLoggingPath
        {
            get { return (string)this["WBTLLoggingPath"]; }
            set { this["WBTLLoggingPath"] = value; }
        }

        [ConfigurationProperty("WBTLWSSPort")]
        public int WBTLWSSPort
        {
            get { return (int)this["WBTLWSSPort"]; }
            set { this["WBTLWSSPort"] = value; }
        }

        [ConfigurationProperty("WBTLWSSName")]
        public string WBTLWSSName
        {
            get { return (string)this["WBTLWSSName"]; }
            set { this["WBTLWSSName"] = value; }
        }
        #endregion

        #region Common
        [ConfigurationProperty("FTPAddress")]
        public string FTPAddress
        {
            get { return (string)this["FTPAddress"]; }
            set { this["FTPAddress"] = value; }
        }
        [ConfigurationProperty("IdleInterval")]
        public ulong IdleInterval
        {
            get { return (ulong)this["IdleInterval"]; }
            set { this["IdleInterval"] = value; }
        }
        #endregion

        public static BasicConfiguration Default
        {
            get
            {
                return new BasicConfiguration()
                {
                    ATLExecutablePath = Environment.CurrentDirectory,
                    ATLLoggingPath = Path.Combine(Environment.CurrentDirectory, "ATL"),
                    WBTLExecutablePath = Environment.CurrentDirectory,
                    WBTLLoggingPath = Path.Combine(Environment.CurrentDirectory, "WBTL"),
                    WBTLWSSName = @"FileHistory",
                    WBTLWSSPort = 4649,
                    IdleInterval = 1000 * 60 * 5,
                    FTPAddress = @"ftp://192.168.1.106/"
                };
            }
        }     

        public BasicConfiguration()
        {

        }

        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(null);

            BasicConfiguration instance = (BasicConfiguration)config.GetSection("BasicConfiguration");

            if (instance == null)
            {
                config.Sections.Add(this.GetType().Name, BasicConfiguration.Default);
                BasicConfiguration.Default.SectionInformation.ForceSave = true;
            }
            else
            {
                instance.ATLExecutablePath = this.ATLExecutablePath;
                instance.ATLLoggingPath = this.ATLLoggingPath;
                instance.WBTLExecutablePath = this.WBTLExecutablePath;
                instance.WBTLLoggingPath = this.WBTLLoggingPath;
                instance.WBTLWSSName = this.WBTLWSSName;
                instance.WBTLWSSPort = this.WBTLWSSPort;
                instance.IdleInterval = this.IdleInterval;
            }


            config.Save(ConfigurationSaveMode.Full);
        }
    }
}
