﻿$(function () {

    function getRenderableCategories() {
        $.get("/Categories/GetRenderableCategories", {}, function (data) {

            $.each(data, function (index, el) {
                addCategory(el.Name, el.Id);
                getCategoryData(el.Id);
            })

        }).fail(function () {
            alert('fail')
        })
    }

    function getCategoryData(id) {
        $.get("/Categories/GetChartDataForCategory", { categoryId: id, from: getYesterdaysDate(), to: "", login: "", forAll: true }, function (data) {
            ChartsRendering.renderPie('#' + id, data);
        }).fail(function () {
            alert('fail')
        })
    }

    function addCategory(name, id) {
        html = ""
        html += '<div class="left half">'
        html += '<h2 class="text-center">' + name + '</h2>'
        html += ' <div id="' + id + '" class="basic-chart"></div></div>'

        $('#charts').append(html);
    }

    function getYesterdaysDate() {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    getRenderableCategories();

});