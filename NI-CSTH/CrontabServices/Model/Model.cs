﻿using CrontabServices.Runer;
using DBModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrontabServices.Model
{
    public class Model
    {

        #region Singleton
        static Model _instance;

        public static Model GetInstance
        {
            get
            {
                return _instance ?? (_instance = new Model());
            }
        }

        private Model()
        {
            this.BackgroundWorker = new BackgroundWorker();
            this.BackgroundWorker.WorkerSupportsCancellation = true;
            this.BackgroundWorker.DoWork += (s, e) =>
            {
                BackgroundWorker bg = s as BackgroundWorker;
                while (true)
                {
                    Console.WriteLine("[Info] Scheduling..");
                    if (bg.CancellationPending)
                        break;
                    this.DoWork();
                    var timeout = this.GetCrontabSchedulerInterval();
                    Console.WriteLine("[Info] Processes completed! Next:{0}",DateTime.Now.AddMilliseconds(timeout));

                    Thread.Sleep(timeout);                   
                }
            };
        }

        private int GetCrontabSchedulerInterval()
        {
            string value = string.Empty;

            using (var db = new DbModelContainer())
            {
                value = db.SystemParameterSet.Where(x => x.Key == "CrontabSchedulerInterval").Single().Value;
            }
            int result;

            if(int.TryParse(value, out result))
            {
                return result * 1000 * 60;
            }
            else
            {
                return 60 * 1000;
            }
        }

        #endregion

        #region Properties
        BackgroundWorker BackgroundWorker { get; set; }
        #endregion
        #region Public Methods
        public void Restart()
        {
            this.Stop();
            this.Start();
        }

        public void Start()
        {
            if (!this.BackgroundWorker.IsBusy)
                this.BackgroundWorker.RunWorkerAsync();
        }



        public void Stop()
        {
            if (this.BackgroundWorker.IsBusy)
                this.BackgroundWorker.CancelAsync();
        }

        #endregion

        #region Private Methods
        private List<Crontab> GetCrontabs()
        {
            List<Crontab> crontabList;
            using (var db = new DbModelContainer())
            {
                crontabList = db.CrontabSet.Where(x => x.IsActive).ToList();
            }

            return crontabList;
        }
        public void RunCrontabByName(string crontabName)
        {
            Crontab crontab = null;

            using (var db = new DbModelContainer())
            {
                try
                {
                    crontab = db.CrontabSet.Where(x => x.Name == crontabName)?.First();
                }
                catch (Exception ex)
                {

                }
            }

            if (crontab != null)
            {
                try
                {
                    Type t = Type.GetType("CrontabServices.Model." + crontabName);

                    CrontabBase crontabcrontabService = (CrontabBase)Activator.CreateInstance(t, crontab);
                    Console.WriteLine("Starting.. " + crontabName);
                    CrontabRuner.Run(crontabcrontabService);
                    this.UpdateCrontab(crontab);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error occured in: " + crontabName);
                }

            }
            else
            {
                Console.WriteLine("Wrong configuration in: " + crontabName);
            }
        }

        private void UpdateCrontab(Crontab crontab)
        {
            using (var db = new DbModelContainer())
            {
                db.CrontabSet.Where(x => x.Name == crontab.Name).Single().LastExecution = DateTime.Now;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Database error occured in: " + crontab.Name);
                    throw;
                }
            }
        }

        private void DoWork()
        {
            List<Crontab> crontabList = new List<Crontab>();
            try
            {
                crontabList = this.GetCrontabs();
            }
            catch (Exception e)
            {
                Console.WriteLine("Database error!");
            }

            crontabList.ForEach(x =>
            {
                this.RunCrontabByName(x.Name);
                //if (x.LastExecution == null)
                //{
                //    this.RunCrontabByName(x.Name);
                //}
                //else if (x.LastExecution?.Add(x.Interval) < DateTime.Now)
                //{
                //    this.RunCrontabByName(x.Name);
                //}
            });
        }
        #endregion
    }
}
