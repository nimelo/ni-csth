﻿using DBModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CrontabServices;

namespace CrontabServices.Model
{
    public class DesktopLogCrontab : CrontabLogBase<AppHistory>
    {
        #region Methods

        protected override List<AppHistory> ExtractLog(string log, History history)
        {
            List<AppHistory> newAppHistory = new List<AppHistory>();
            List<string> domains = new List<string>();

            var splittedLog = log.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (splittedLog.Length % 3 != 0) throw new Exception();

            for (int i = 0; i < (splittedLog.Length / 3) * 3; i += 3)
            {

                DateTime begin = DateTime.Parse(splittedLog[i]);

                var process = splittedLog[i + 1].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string pn = process[0];
                string path = process[1];
                //TODO missing file directory
                DateTime end = DateTime.Parse(splittedLog[i + 2]);

                var appItemPeriod = new AppItemPeriod()
                {
                    From = begin,
                    To = end,
                    TimeSpan = end - begin,
                };

                var appHistory = new AppHistory()
                {
                    ProcessName = pn,
                    Path = path,
                    HistoryId = history.Id,
                    AppItemPeriod = appItemPeriod
                };

                if (domains.Contains(path))
                {
                    appHistory.AppHistoryDomain = newAppHistory.Where(x => x.Path == path).First().AppHistoryDomain;
                    newAppHistory.Where(x => x.Path == path).First().AppHistoryDomain.AppItemPeriods.Add(appItemPeriod);
                    newAppHistory.Where(x => x.Path == path).First().AppHistoryDomain.SumTimeSpan += appItemPeriod.TimeSpan;
                }
                else
                {
                    var appDomain = new AppHistoryDomain()
                    {
                        Path = path,
                        ProcessName = pn,
                        AppItemPeriods = new List<AppItemPeriod>() { appItemPeriod },
                        AppHistories = new List<AppHistory>() { appHistory },
                        HistoryId = history.Id,
                        SumTimeSpan = appItemPeriod.TimeSpan
                    };

                    appHistory.AppHistoryDomain = appDomain;

                    domains.Add(path);
                }


                newAppHistory.Add(appHistory);
            }

            return newAppHistory;
        }

        protected override void SaveToDB(List<AppHistory> newWebHistory, int historyId)
        {
            using (var db = new DbModelContainer())
            {
                db.AppHistorySet.AddRange(newWebHistory);
                db.HistorySet.Where(x => x.Id == historyId).Single().ExistAppHistories = true;

                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        protected override IEnumerable<string> SeparateApprovedFiles(string[] files)
        {
            return files.Where(f =>
            {
                return this.UserHistories.Where(us => us.ExistAppHistories == false)
                            .Select(s => s.Day.ToShortDateString())
                            .Contains(Path.GetFileName(f).Replace(".log", string.Empty));

            }).ToList();
        }

        #endregion

        #region Ctrs
        public DesktopLogCrontab(Crontab crontab) : base(crontab)
        {
        }
        #endregion
    }
}
