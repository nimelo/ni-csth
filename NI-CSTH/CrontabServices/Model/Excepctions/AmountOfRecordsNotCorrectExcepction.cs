﻿using System;
using System.Runtime.Serialization;

namespace CrontabServices.Model
{
    [Serializable]
    internal class AmountOfRecordsNotCorrectExcepction : Exception
    {
        public AmountOfRecordsNotCorrectExcepction()
        {
        }

        public AmountOfRecordsNotCorrectExcepction(string message) : base(message)
        {
        }

        public AmountOfRecordsNotCorrectExcepction(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AmountOfRecordsNotCorrectExcepction(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}