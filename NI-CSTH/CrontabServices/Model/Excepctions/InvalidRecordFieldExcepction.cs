﻿using System;
using System.Runtime.Serialization;

namespace CrontabServices.Model
{
    [Serializable]
    public class InvalidRecordFieldExcepction : Exception
    {
        private string name;
        private string v;

        public InvalidRecordFieldExcepction()
        {
        }

        public InvalidRecordFieldExcepction(string message) : base(message)
        {
        }

        public InvalidRecordFieldExcepction(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidRecordFieldExcepction(string name, string v)
        {
            this.name = name;
            this.v = v;
        }

        protected InvalidRecordFieldExcepction(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string ToString()
        {
            return $"Invalid type:{this.v} in filed:{this.name}";
        }
    }
}