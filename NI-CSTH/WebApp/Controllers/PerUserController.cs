﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class PerUserController : Controller
    {
        // GET: PerUser
        public ActionResult Index()
        {
            return View("PerUserHomePartial");
        }

        public ActionResult ExistUser(string firstName, string lastName)
        {
            var users = new List<Account>();
            using (var db = new DbModelContainer())
            {
                db.AccountSet.Include("User").Where(x => x.FirstName == firstName && x.LastName == lastName).ToList().ForEach(x=> users.Add(new Account()
                {
                    FirstName  = x.FirstName,
                    LastName = x.LastName,
                    User = new User()
                    {
                        IsActive = x.User.IsActive,
                        Login = x.User.Login
                    }
                }));
            }           

            return Json(users, JsonRequestBehavior.AllowGet);
        }
    }
}