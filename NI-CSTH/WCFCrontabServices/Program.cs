﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (new SingleGlobalInstance(1000)) 
                {
                    Console.WriteLine("Main instance!");
                    //ATL.StartPipeServer();
                    CommandLineParser.Parse("-start");
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Other instance!");
                if(args.Length == 1)
                   // ATL.NotifyPipeServer(args[0]);

                Environment.Exit(0);
            }

        }
    }
}