﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Configurations
{
    public class ATLConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("LoggingPath")]
        public string LoggingPath
        {
            get { return (string)this["LoggingPath"]; }
            set { this["LoggingPath"] = value; }
        }

        [ConfigurationProperty("IdleInterval")]
        public ulong IdleInterval
        {
            get { return (ulong)this["IdleInterval"]; }
            set { this["IdleInterval"] = value; }
        }

        public static ATLConfiguration Default
        {
            get
            {
                return new ATLConfiguration()
                {
                    LoggingPath = Environment.CurrentDirectory,
                    IdleInterval = 1000 * 60 * 5
                };
            }
        }

        public ATLConfiguration()
        {

        }

        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(null);

            ATLConfiguration instance = (ATLConfiguration)config.GetSection(this.GetType().Name);

            if (instance == null)
            {
                config.Sections.Add(this.GetType().Name, ATLConfiguration.Default);
                ATLConfiguration.Default.SectionInformation.ForceSave = true;
            }
            else
            {
                instance.LoggingPath = this.LoggingPath;
                instance.IdleInterval = this.IdleInterval;
            }

            this.CreateDirectory((ATLConfiguration)config.GetSection(this.GetType().Name));

            config.Save(ConfigurationSaveMode.Full);
        }

        private void CreateDirectory(ATLConfiguration instance)
        {
            if (!Directory.Exists(instance.LoggingPath))
            {
                Directory.CreateDirectory(instance.LoggingPath);
            }
        }
    }
}
