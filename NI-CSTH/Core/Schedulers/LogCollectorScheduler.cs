﻿using ManagmentApplication.Classes.ProcessHelper;
using ManagmentApplication.Communications;
using ManagmentApplication.WCFControlServices.ScheduleService;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ManagmentApplication.Schedulers
{
    public class LogCollectorScheduler
    {
        public static void Start(UserSchedule schedule)
        {
            var config = MA.Config;
            foreach (var date in schedule.DesktopLogList)
            {
                var path = Path.Combine(config.ATLLoggingPath, DateTimeToFileName(date));
                if (File.Exists(path))
                {
                    try
                    {
                        FTPHelper.UploadFile(path, Path.Combine(MA.Login, "inApp", DateTimeToFileName(date)));
                        Uploaded(date, true, false);
                    }
                    catch (Exception e)
                    {
                        throw;
                    }                
                }
            }

            foreach (var date in schedule.WebLogList)
            {
                var path = Path.Combine(config.WBTLLoggingPath, DateTimeToFileName(date));
                if (File.Exists(path))
                {
                    try
                    {
                        FTPHelper.UploadFile(path, Path.Combine(MA.Login, "inWeb", DateTimeToFileName(date)));
                        Uploaded(date, false, true);
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
            }
        }

        private static void Uploaded(DateTime day, bool app, bool web)
        {
            using (var service = new ScheduleServiceClient())
            {
                //TODO Schedule
                service.Uploaded(MA.Login, day, app, web);
            }
        }

        private static string DateTimeToFileName(DateTime date)
        {
            return string.Format("{0}.log", date.ToShortDateString());
        }
    }
}
