﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class CategoriesEditController : Controller
    {
        // GET: CategoriesEdit
        public ActionResult Index()
        {
            return View("CategoriesHomePartial");
        }
        [HttpPost]
        public ActionResult AddCategory(string name, bool isRenderable)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategorySet.Where(x => x.Name == name).Count() > 0)
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.CategorySet.Add(new Category()
                    {
                        Name = name,
                        IsRenderable = isRenderable
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }

            }

            return Json(string.Empty);
        }

        public ActionResult GetCategories()
        {
            List<Category> categories = new List<Category>();
            using (var db = new DbModelContainer())
            {
                db.CategorySet.ToList().ForEach(x => categories.Add(new Category()
                {
                    Name = x.Name,
                    IsRenderable = x.IsRenderable,
                    Id = x.Id,
                }));
            }

            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCategoryItemsBy(string name)
        {
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            using (var db = new DbModelContainer())
            {
                int categoryId = db.CategorySet.Where(x => x.Name == name).First().Id;
                categoryItems = db.CategoryItemSet.Where(x => x.CategoryId == categoryId).ToList();
            }

            return Json(categoryItems, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCategoryItemsByCategoryId(int categoryId)
        {
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            using (var db = new DbModelContainer())
            {
                db.CategoryItemSet.Where(x => x.CategoryId == categoryId).ToList().ForEach(x => categoryItems.Add(new CategoryItem() { UniqueIdentificator = x.UniqueIdentificator, CategoryId = x.CategoryId, Id = x.Id, Type = x.Type }));
            }

            return Json(categoryItems, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditCategory(int categoryId, string name, bool isRenderable)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategorySet.Any(x => x.Name == name  && x.Id != categoryId) || !db.CategorySet.Any(x => x.Id == categoryId))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    var category = db.CategorySet.Where(x => x.Id == categoryId).First();
                    category.Name = name;
                    category.IsRenderable = isRenderable;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }

            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult AddCategoryItem(int categoryId, byte itemType, string identifier)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategoryItemSet.Any(x => x.CategoryId == categoryId && x.UniqueIdentificator == identifier && x.Type == itemType))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.CategoryItemSet.Add(new CategoryItem()
                    {
                        CategoryId = categoryId,
                        UniqueIdentificator = identifier,
                        Type = itemType
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }
            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult DeleteCategoryItem(int categoryId, int id)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategoryItemSet.Any(x => x.CategoryId == categoryId && x.Id == id))
                {
                    try
                    {
                        db.CategoryItemSet
                        .Remove(db.CategoryItemSet
                                    .Where(x => x.CategoryId == categoryId && x.Id == id)
                                    .First());
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 303;
                    }
                }
                else
                {
                    Response.StatusCode = 303;
                }
            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult DeleteCategory(int categoryId)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategorySet.Any(x => x.Id == categoryId))
                {

                    try
                    {
                        db.CategoryItemSet
                        .RemoveRange(db.CategoryItemSet
                                    .Where(x => x.CategoryId == categoryId));
                        db.CategorySet.Remove(db.CategorySet.Where(x => x.Id == categoryId).First());
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 303;
                    }
                }
                else
                {
                    Response.StatusCode = 303;
                }
            }

            return Json(string.Empty);
        }


    }
}
