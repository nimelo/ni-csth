﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace WebApp.Controllers
{
    public class CrontabsController : Controller
    {
        // GET: Crontabs
        public ActionResult Index()
        {
            return View("CrontabsHomePartial");
        }


        public ActionResult GetCrontabStatuses()
        {
            List<object> crontabs = new List<object>();
            using (var db = new DbModelContainer())
            {
                db.CrontabSet.Include("FileProcessingHistory").Include("FileProcessingHistory.Account").Include("FileProcessingHistory.Account.User")
                    .ToList().ForEach(x =>
                        crontabs.Add(new
                        {
                            Name = x.Name,
                            Processed = x.FileProcessingHistory.Count(),
                            Good = x.FileProcessingHistory.Where(y => y.Status == 0).Count(),
                            Fail = x.FileProcessingHistory.Where(y => y.Status != 0).Count(),
                            Errors = x.FileProcessingHistory.Where(y => y.Status != 0).ToList().Select(z =>
                             {
                                 return new { Login = z.Account.User.Login, DateOfFile = z.Name, ProcessingDate = z.ProcessingDate };
                             })
                        }));
            }

            return Json(crontabs, JsonRequestBehavior.AllowGet);
        }
    }
}