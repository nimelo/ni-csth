﻿using ManagmentApplication.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication
{
    public static class MA
    {
        public static BasicConfiguration Config { get; internal set; }

        public static void Initialize()
        {
            Config = ConfigurationManager.GetSection("BasicConfiguration") as BasicConfiguration;     
            if(Config == null)
            {
                Config = BasicConfiguration.Default;
                Config.Save();
            }      
        }

        public static string Login { get; set; }

        public static string Password { get; set; }
    }
}
