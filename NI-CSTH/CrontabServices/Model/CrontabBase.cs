﻿using DBModel;

namespace CrontabServices.Model
{
    public abstract class CrontabBase
    {
        public Crontab Crontab { get; protected set; }

        public abstract void DoWork();

        public CrontabBase(Crontab crontab)
        {
            this.Crontab = crontab;
        }
    }
}