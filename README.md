# Uatu, often simply known as The Watcher, is a fictional character that appears in comic books published by Marvel Comics. #
The main idea of this project is to get know how people are using their PCs. It is very common that we spend a lot of time on games, websides or working.
UATU gives you opportunity to check how much time do you spend on:


* Particular programs
* Particular web-sides like (Facebook, YT, Twitter) time is cumulated for sub-domains.
* You can always write your own C# module for desktop part of the application. The simple example shows how to write the sub-module that checks your hardware usage.

# This project is a first version of an UATU software which can be accessed under following link: [UATU 2.0](https://bitbucket.org/nimelo/uatu/overview)#