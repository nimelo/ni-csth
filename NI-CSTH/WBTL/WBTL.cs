﻿using WBTL.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBTL
{
    public class WBTL
    {
        #region Fields

        private static WBTLConfiguration _config;

        #endregion
        #region Properties

        public static WBTLConfiguration Config
        {
            get
            {
                if (_config == null)
                {
                    return ConfigurationManager.GetSection(typeof(WBTLConfiguration).Name) as WBTLConfiguration;
                }
                else
                    return _config;
            }
        }

        #endregion

        #region Methods

        public static void Initialize()
        {
            var config = ConfigurationManager.GetSection(typeof(WBTLConfiguration).Name) as WBTLConfiguration;
            if (config == null)
            {
                config = WBTLConfiguration.Default;
                config.Save();
                _config = config;
            }
        }

        public static void StartPipeServer()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    using (var server = new NamedPipeServerStream("WBTL"))
                    {
                        server.WaitForConnection();
                        using (var reader = new StreamReader(server))
                        {
                            var line = reader.ReadLine();
                            if (line != null)
                                CommandLineParser.Parse(line);
                        }
                    }
                }
            });
        }

        public static void NotifyPipeServer(string toNotify)
        {
            using (var client = new NamedPipeClientStream("WBTL"))
            {
                client.Connect();
                using (StreamWriter writer = new StreamWriter(client))
                {
                    writer.WriteLine(toNotify);
                    writer.Flush();
                }
            }
        }

        #endregion
    }
}
