﻿using CrontabServices.Model.ImportCrontabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBModel;

namespace CrontabServices.Model
{
    public class FellPresenceImportCrontab : CrontabImportBase<FellPresenceImportRecord>
    {
        public FellPresenceImportCrontab(Crontab crontab) : base(crontab)
        {
        }

        protected override void SaveToDb(FellPresenceImportRecord record)
        {
            using (var db = new DbModelContainer())
            {
                var account = db.AccountSet.Include("User")
                 .Where(x => x.FirstName == record.FirstName
                             && x.LastName == record.LastName
                             && x.User.Login == record.Login).SingleOrDefault();

                List<History> fellPresenceHistories = new List<History>();

                for (DateTime i = record.From; i <= record.To; i = i.AddDays(1))
                {
                    fellPresenceHistories.Add(new History()
                    {
                        AccountId = account.Id,
                        ExistAppHistories = true,
                        ExistWebHistories = true,
                        ExistAppFtp = true,
                        ExistWebFtp = true,
                        Day = i,
                    });
                }

                db.HistorySet.AddRange(fellPresenceHistories);

                db.SaveChanges();

            }
        }
    }
}
