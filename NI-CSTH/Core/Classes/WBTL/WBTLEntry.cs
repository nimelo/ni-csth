﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Classes.WBTL
{
    public class WBTLEntry
    {
        #region Properties
        public string Url { get; set; }
        #endregion

        #region Methods
        public override bool Equals(object obj)
        {
            if(obj != null && obj.GetType() == typeof(WBTLEntry))
            {
                return ((WBTLEntry)obj).GetDomain == this.GetDomain;
            }

            return false;
        }

        public string GetDomain
        {
            get
            {
                string domain = string.Empty;

                if (this.Url.IndexOf("://") > -1)
                {
                    domain = this.Url.Split('/')[2];
                }
                else
                {
                    domain = this.Url.Split('/')[0];
                }

                domain = domain.Split(':')[0];

                return domain;
            }
        }
        #endregion

        public WBTLEntry(string url)
        {
            this.Url = url;
        }

        
    }
}
