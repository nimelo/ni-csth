﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Classes.ATL
{
    public class ATLEntry
    {
        #region Properties
        public string ProcessName { get; set; }

        public string Path { get; set; }
        #endregion

        #region Ctors
        public ATLEntry()
        {
        }
        #endregion

        #region Public Methods
        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == typeof(ATLEntry))
            {
                if (((ATLEntry)obj).ProcessName == this.ProcessName)
                {
                    if (((ATLEntry)obj).Path == this.Path)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public override string ToString()
        {
            return this.GetType().GetProperties().ToList().Select(x => x.GetValue(this).ToString()).Aggregate((s1, s2) => s1 + s2);
        }
        #endregion
    }
}
