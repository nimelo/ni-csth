﻿using Common;
using ManagmentApplication.Classes;
using ManagmentApplication.Classes.ATL;
using ManagmentApplication.Classes.ProcessHelper;
using ManagmentApplication.Classes.WBTL;
using ManagmentApplication.LogCollectors;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ManagmentApplication.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Fields
        private LoginControlViewModel _loginControlViewModel;
        private List<object> _desktopStatisticsCollection;
        private List<object> _webStatisticsCollection;
        #endregion

        #region Properties
        public LoginControlViewModel LoginControlViewModel
        {
            get { return _loginControlViewModel; }
            set
            {
                _loginControlViewModel = value;
                this.NotifyPropertyChanged();
            }
        }

        public List<object> DesktopStatisticsCollection
        {
            get
            {
                return GetTodaysDesktopStatisticsCollection().ToList();
            }
            set
            {
                this.NotifyPropertyChanged();
            }
        }

        //http://stackoverflow.com/questions/27036234/adding-chartseries-programmatically-mvvm-in-modern-ui-metro-charts
        public List<object> WebStatisticsCollection
        {
            get
            {
                return GetTodaysWebStatisticsCollection().ToList();
            }
            set
            {
                this.NotifyPropertyChanged();
            }
        }
        #endregion

        #region Model

        #endregion

        #region ViewModels

        #endregion

        #region Contents

        #endregion

        #region Ctors
        public MainWindowViewModel()
        {
            MA.Initialize();
            this.LoginControlViewModel = new LoginControlViewModel();
            //ProcessHelper.StartATL();
        }

        #endregion

        #region Commands


        #endregion
        #region Handlers


        #endregion

        #region Private methods

        private IEnumerable<object> EvaluateDesktopStatistics(string log)
        {
            Dictionary<ATLEntry, TimeSpan> programs = new Dictionary<ATLEntry, TimeSpan>(new ATLEqualityComparer());

            try
            {
                programs = EvaluateDesktopLog(log);
            }
            catch
            {

            }

            //TODO 
            return programs.ToList().Select(x => new { Key = x.Key.ProcessName, Value = Math.Round(x.Value.TotalSeconds / 60.0, 2) }).ToList().OrderByDescending(x => x.Value).Take(6);
        }

        private Dictionary<ATLEntry, TimeSpan> EvaluateDesktopLog(string log)
        {
            Dictionary<ATLEntry, TimeSpan> programs = new Dictionary<ATLEntry, TimeSpan>(new ATLEqualityComparer());

            var splittedLog = log.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            //if (splittedLog.Length % 10 != 0) throw new Exception();

            for (int i = 0; i < (splittedLog.Length / 3) * 3; i += 3)
            {
                try
                {
                    DateTime begin = DateTime.Parse(splittedLog[i]);

                    var process = splittedLog[i + 1].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    string pn = process[0];
                    string path = process[1];
                    //TODO missing file directory
                    DateTime end = DateTime.Parse(splittedLog[i + 2]);

                    var atlEntry = new ATLEntry()
                    {
                        ProcessName = pn,
                        Path = path

                    };

                    var amountOfTime = end - begin;

                    if (programs.Keys.Contains(atlEntry))
                    {
                        programs[atlEntry] += amountOfTime;
                    }
                    else
                    {
                        programs.Add(atlEntry, amountOfTime);
                    }
                }
                catch (Exception e)
                {

                }
            }

            return programs;
        }

        private IEnumerable<object> EvaluateWebStatistics(string log)
        {
            Dictionary<WBTLEntry, int> domains = new Dictionary<WBTLEntry, int>();

            try
            {
                domains = EvaluateWebLog(log);
            }
            catch
            {

            }

            //TODO
            return domains.ToList().Select(x => new { Key = x.Key.GetDomain, Value = Math.Round(x.Value / 60.0, 2) }).ToList().OrderByDescending(x => x.Value).Take(6);
        }

        private Dictionary<WBTLEntry, int> EvaluateWebLog(string log)
        {
            Dictionary<WBTLEntry, int> domains = new Dictionary<WBTLEntry, int>(new WBTLEqualityComparer());

            var splittedLog = log.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (splittedLog.Length % 4 != 0) throw new Exception();

            for (int i = 0; i < splittedLog.Length; i += 4)
            {
                var wbtlEntry = new WBTLEntry(splittedLog[i + 1]);
                var amountOfTime = int.Parse(splittedLog[i + 2]);

                if (domains.Keys.Contains(wbtlEntry))
                {
                    domains[wbtlEntry] += amountOfTime;
                }
                else
                {
                    domains.Add(wbtlEntry, amountOfTime);
                }

            }

            return domains;
        }
        #endregion

        #region Public methods

        public IEnumerable<object> GetTodaysDesktopStatisticsCollection()
        {
            string path = MA.Config.ATLLoggingPath;//@"E:\!@StartUps\ATL";
            //@"C:\Users\Mateusz\AppData\Roaming\ATL\" + @"05.09.2015.log");
            var log = LogCollector.GetInstance.Collect(Path.Combine(path, string.Format("{0}.log", DateTime.Today.ToShortDateString())));

            return this.EvaluateDesktopStatistics(log);
        }

        public IEnumerable<object> GetTodaysWebStatisticsCollection()
        {                                      //@"C:\Users\Mateusz\AppData\Roaming\ATL\" + @"05.09.2015.log");
            string path = MA.Config.WBTLLoggingPath;//@"E:\!@StartUps\WBTL";
            var log = LogCollector.GetInstance.Collect(Path.Combine(path, string.Format("{0}.log", DateTime.Today.ToShortDateString())));

            return this.EvaluateWebStatistics(log);
        }

        public void Refreash()
        {
            this.DesktopStatisticsCollection = null;
            this.WebStatisticsCollection = null;
        }
        #endregion


    }
}

