﻿using System;
using System.Runtime.Serialization;

namespace CrontabServices.Model
{
    [Serializable]
    public class InvalidHeaderExcepction : Exception
    {
        public InvalidHeaderExcepction()
        {
        }

        public InvalidHeaderExcepction(string message) : base(message)
        {
        }

        public InvalidHeaderExcepction(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidHeaderExcepction(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}