﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WBTL.Queue;
using WebSocketSharp;
using WebSocketSharp.Net.WebSockets;
using WebSocketSharp.Server;

namespace WBTL.Models
{
    //TODO propably add event when new log comes out using stack.
    public class FileHistory : WebSocketBehavior
    {
        #region Fields
        private WebSocket _webBrowserSocket;
        #endregion

        #region Properties
        public WebSocket WebBrowserSocket
        {
            get
            {
                return _webBrowserSocket;
            }
            private set
            {
                _webBrowserSocket = value;
            }
        }
        #endregion

        public FileHistory() : base()
        {
            _instance = this;
        }

        #region Singleton

        static FileHistory _instance;

        public static FileHistory GetInstance
        {
            get
            {
                return _instance ?? (_instance = new FileHistory());
            }
        }
        #endregion

        #region Methods
        protected override void OnOpen()
        {
            Console.WriteLine("Someone connected!");
            base.OnOpen();
            //this.WebBrowserSocket = this.Context.WebSocket;
            //this.AskForLog();
        }

        protected override void OnClose(CloseEventArgs e)
        {
            //this.WebBrowserSocket = null;
            base.OnClose(e);
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            Console.WriteLine("Someone send message!");

            File.AppendAllText(Path.Combine(WBTL.Config.LoggingPath, string.Format(@"{0}{1}", DateTime.Today.ToShortDateString(), ".log")), e.Data);

            Console.WriteLine(e.Data);
            base.OnMessage(e);
        }

        public void AskForLog()
        {
            FileList.GetInstance.List.ForEach(x => this.WebBrowserSocket.Send("READ_" + this.PrepareFileName(x)));            
        }

        private string PrepareFileName(DateTime date)
        {
            return $"!{date.Year}-{date.Month}-{date.Day}.dat";
        }

        #endregion

    }
}
