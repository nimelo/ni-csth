﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices.Model
{
    public class HistoryUpgradeCrontab : CrontabBase
    {
        public override void DoWork()
        {
            using (var db = new DbModelContainer())
            {
                List<int> accountIds = db.AccountSet.Where(x => x.User.IsActive && !x.History.Any(y=>y.Day.Equals(DateTime.Today))).Select(y => y.Id).ToList();
                var newHistories = this.UpgradeHistories(accountIds, DateTime.Today);

                db.HistorySet.AddRange(newHistories);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private IEnumerable<History> UpgradeHistories(IEnumerable<int> accountIds, DateTime day)
        {
            foreach (var id in accountIds)
            {
                yield return new History()
                {
                    Day = day,
                    AccountId = id
                };
            }
        }

        public HistoryUpgradeCrontab(Crontab crontab) : base(crontab)
        {

        }
    }
}
