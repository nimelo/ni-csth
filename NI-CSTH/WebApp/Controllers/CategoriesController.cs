﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace WebApp.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Categories
        public ActionResult Index()
        {
            return View("CategoriesHomePartial");
        }

        public ActionResult Test()
        {
            return Json("test", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddCategory(string name, bool isRenderable)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategorySet.Where(x => x.Name == name).Count() > 0)
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.CategorySet.Add(new Category()
                    {
                        Name = name,
                        IsRenderable = isRenderable
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }

            }

            return Json(string.Empty);
        }

        [HttpPost]
        [HttpGet]
        public ActionResult GetCategories()
        {
            List<Category> categories = new List<Category>();
            using (var db = new DbModelContainer())
            {
                db.CategorySet.ToList().ForEach(x => categories.Add(new Category()
                {
                    Name = x.Name,
                    IsRenderable = x.IsRenderable,
                    Id = x.Id,
                }));
            }

            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLastCategoryApp(string login)
        {
            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            List<object> returnList = new List<object>();
            List<AppHistoryDomain> histories = new List<AppHistoryDomain>();
            Dictionary<string, string> mapping = new Dictionary<string, string>();
            using (var db = new DbModelContainer())
            {
                var date = DateTime.Today.AddDays(-1);
                db.Configuration.LazyLoadingEnabled = false;
                histories = db.HistorySet
                                    .Include("AppHistoryDomain")
                                    .Include("Account")
                                    .Include("Account.User")
                                    .Where(h => h.Day == date && h.Account.User.Login == login).SelectMany(s => s.AppHistoryDomain).OrderByDescending(ob => ob.SumTimeSpan).ToList();
                mapping = db.AppPathMapSet.ToDictionary(x => x.Path, x => x.Name);
            }

            histories.Take(10).ToList().ForEach(x =>
            {
                if (mapping.Keys.Contains(x.Path))
                {
                    dic.Add(mapping[x.Path], x.SumTimeSpan);
                }
                else
                {
                    dic.Add(x.Path, x.SumTimeSpan);
                }                
            });

            dic.Add("Others", histories.Skip(10).ToList().Aggregate(TimeSpan.Zero, (a, b) => a + b.SumTimeSpan));

            dic.ToList().ForEach(x => returnList.Add(new
            {
                name = x.Key,
                y = x.Value.TotalSeconds
            }));

            return Json(returnList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLastCategoryWeb(string login)
        {
            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            List<object> returnList = new List<object>();
            List<WebHistoryDomain> histories = new List<WebHistoryDomain>();
            using (var db = new DbModelContainer())
            {
                var date = DateTime.Today.AddDays(-1);
                db.Configuration.LazyLoadingEnabled = false;
                histories = db.HistorySet
                                    .Include("WebHistoryDomain")
                                    .Include("Account")
                                    .Include("Account.User")
                                    .Where(h => h.Day == date && h.Account.User.Login == login).SelectMany(s => s.WebHistoryDomain).OrderByDescending(ob=> ob.SumTimeSpan).ToList();
            }

            histories.Take(10).ToList().ForEach(x =>
            {
                dic.Add(x.Domain, x.SumTimeSpan);
            });

            dic.Add("Others", histories.Skip(10).ToList().Aggregate(TimeSpan.Zero, (a, b) => a + b.SumTimeSpan));
            
            dic.ToList().ForEach(x => returnList.Add(new
            {
                name = x.Key,
                y = x.Value.TotalSeconds
            }));

            return Json(returnList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCategoryItemsBy(string name)
        {
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            using (var db = new DbModelContainer())
            {
                int categoryId = db.CategorySet.Where(x => x.Name == name).First().Id;
                categoryItems = db.CategoryItemSet.Where(x => x.CategoryId == categoryId).ToList();
            }

            return Json(categoryItems, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddCategoryItem(string categoryName, byte itemType, string identifier)
        {
            using (var db = new DbModelContainer())
            {
                var categoryId = db.CategorySet.Where(x => x.Name == categoryName).First().Id;
                if (db.CategoryItemSet.Any(x => x.CategoryId == categoryId && x.UniqueIdentificator == identifier && x.Type == itemType))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.CategoryItemSet.Add(new CategoryItem()
                    {
                        CategoryId = categoryId,
                        UniqueIdentificator = identifier,
                        Type = itemType
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }
            }

            return Json(string.Empty);
        }

        public ActionResult GetChartDataForCategory(int categoryId, string from, string to, string login, bool forAll)
        {
            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            Dictionary<string, string> mapping = new Dictionary<string, string>();
            List<string> logins = new List<string>();
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            using (var db = new DbModelContainer())
            {
                logins = db.UserSet.Select(x => x.Login).ToList();
                categoryItems = db.CategoryItemSet.Where(x => x.CategoryId == categoryId).ToList();
                mapping = db.AppPathMapSet.ToDictionary(x => x.Path, x => x.Name);
            }
            if (forAll)
            {               
                categoryItems.ForEach(x => dic.Add(x.UniqueIdentificator, new TimeSpan()));

                logins.ForEach(x =>
                {
                    var tmpDic = GetDataForCategory(x, categoryId, from, to);
                    tmpDic.ToList().ForEach(y => dic[y.Key] += y.Value);
                });
            }
            else
            {
                dic = GetDataForCategory(login, categoryId, from, to);
            }

            List<object> returnList = new List<object>();

            dic.ToList().ForEach(x =>
            {
                if (mapping.Keys.Contains(x.Key))
                {
                    returnList.Add(new
                    {
                        name = mapping[x.Key],
                        y = x.Value.TotalSeconds
                    });
                }
                else
                {
                    returnList.Add(new
                    {
                        name = x.Key,
                        y = x.Value.TotalSeconds
                    });
                }
                
            });

            return Json(returnList, JsonRequestBehavior.AllowGet);
        }


        private Dictionary<string, TimeSpan> GetDataForCategory(string login, int categoryId, string from, string to)
        {
            DateTime fromDate;
            DateTime toDate;

            try
            {
                fromDate = DateTime.ParseExact(from, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }
            catch (Exception)
            {
                fromDate = DateTime.MinValue;
            }

            try
            {
                toDate = DateTime.ParseExact(to, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }
            catch (Exception)
            {

                toDate = DateTime.MaxValue;
            }

            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            List<History> histories = new List<History>();

            using (var db = new DbModelContainer())
            {
                db.Configuration.LazyLoadingEnabled = false;

                histories = db.HistorySet
                    .Include("WebHistoryDomain")
                    .Include("AppHistoryDomain")
                    .Where(h => h.Day >= fromDate && h.Day <= toDate && h.Account.User.Login == login).ToList();
                categoryItems = db.CategoryItemSet.Where(x => x.CategoryId == categoryId).ToList();
            }

            return ProcessHistory(categoryItems, histories);
        }

        private Dictionary<string, TimeSpan> ProcessHistory(List<CategoryItem> categoryItems, List<History> histories)
        {
            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            categoryItems.ForEach(x => dic.Add(x.UniqueIdentificator, new TimeSpan()));

            categoryItems.ForEach(ci =>
            {
                if (ci.Type == 0)
                {
                    histories.Where(h => h.ExistAppHistories)
                                .SelectMany(whd => whd.AppHistoryDomain
                                                        .Where(d => d.Path == ci.UniqueIdentificator)).ToList()
                                                        .ForEach(x =>
                                                        {
                                                            dic[x.Path] += x.SumTimeSpan;
                                                        });
                }
                else if (ci.Type == 1)
                {
                    histories.Where(h => h.ExistWebHistories)
                                 .SelectMany(whd => whd.WebHistoryDomain
                                                         .Where(d => d.Domain == ci.UniqueIdentificator)).ToList()
                                                         .ForEach(x =>
                                                         {
                                                             dic[x.Domain] += x.SumTimeSpan;
                                                         });
                }
            });

            return dic;
        }

        public ActionResult GetRenderableCategories()
        {
            List<Category> categories = new List<Category>();
            using (var db = new DbModelContainer())
            {
                db.CategorySet.Where(z => z.IsRenderable).ToList().ForEach(x => categories.Add(new Category()
                {
                    Name = x.Name,
                    IsRenderable = x.IsRenderable,
                    Id = x.Id,
                }));
            }

            return Json(categories, JsonRequestBehavior.AllowGet);
        }

    }
}