﻿$(function () {

    function tableClick() {
        $('#crontabParametersContainer table tbody tr').click(function () {

            $('.edit').remove();

            if (!$(this).hasClass('edit')) {
                if (!$(this).next().hasClass('edit')) {
                    $(this).after(renderCrontabParametersTR(rowCrontabParametersToJson($(this)[0])))
                    me = $(this)

                    atrs = rowCrontabParametersToJson($(this)[0]);
                    $('#inIntervalCP').val(atrs.interval);
                    $('#inInputFolderNameCP').val(atrs.inputFolderName);
                    $('#inOutputFolderNameCP').val(atrs.outputFolderName);
                    $('#inErrorFolderNameCP').val(atrs.errorFolderName);
                    $('#inRootPathCP').val(atrs.rootPath);
                    $('#inIsActiveCP').val((atrs.isActive == 'Yes'? 1:0));

                    $('.saveCrontabParameters').click(function () {
                        crontabId = Number(me.closest('tr').attr('cid'));
                        interval = $('#inIntervalCP').val();
                        input = $('#inInputFolderNameCP').val();
                        output = $('#inOutputFolderNameCP').val();
                        error = $('#inErrorFolderNameCP').val();
                        root = $('#inRootPathCP').val();
                        isActive = Number($('#inIsActiveCP').val());
                        modifyCrontabParameter(crontabId, interval, input, output, error, root, isActive, function () {
                            me.closest('tr')
                                .removeClass('alert-danger')
                                .addClass('alert-success');

                            setTimeout(getCrontabParameters(renderCrontabParameters), 2000);
                        }, function () {
                            me.closest('tr')
                                    .addClass('alert-danger')
                        })


                    })
                }
            }
        })
    }

    function rem(el) {
        el.remove();
    }

    function renderCrontabParametersTR(atrs) {
        var html = "";
        html += '<tr class="edit alert alert-danger" cid="' + atrs.id + '">'

        html += '<td>' + atrs.name + '</td>'

        html += '<td>' + '<input id="inIntervalCP" type="text" class="form-control" />' + '</td>'
        html += '<td>' + '<input id="inInputFolderNameCP" type="text" class="form-control" />' + '</td>'
        html += '<td>' + '<input id="inErrorFolderNameCP" type="text" class="form-control" />' + '</td>'
        html += '<td>' + '<input id="inRootPathCP" type="text" class="form-control" />' + '</td>'
        html += '<td>' + '<input id="inOutputFolderNameCP" type="text" class="form-control" />' + '</td>'
       
        html += '<td>' + '<select id="inIsActiveCP" class="form-control" style="min-width:80px"><option value="1">Yes</option><option value="0">No</option></select>' + '</td>'

        html += '<td>' + '<button class="saveCrontabParameters btn glyphicon glyphicon-ok-circle"></button>' + '</td>'

        html += '</tr>'

        return html;
    }

    function rowCrontabParametersToJson(row) {
        var json = Object();
        json.id = row.getAttribute('cid');
        json.name = row.querySelector('td:nth-child(1)').innerHTML
        json.interval = row.querySelector('td:nth-child(2)').innerHTML
        json.inputFolderName = row.querySelector('td:nth-child(3)').innerHTML
        json.errorFolderName = row.querySelector('td:nth-child(4)').innerHTML
        json.rootPath = row.querySelector('td:nth-child(5)').innerHTML
        json.outputFolderName = row.querySelector('td:nth-child(6)').innerHTML
        json.isActive = row.querySelector('td:nth-child(7)').innerHTML

        return json;
    }

    function modifyCrontabParameter(crontabId, interval, input, output, error, root, isActive, callback, callbackFail) {
        $.ajax({
            url: "/CrontabParameters/ModifyCrontabParameter",
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ crontabId: crontabId, interval: interval, input: input, output: output, error: error, root: root, isActive: isActive }),
            async: true,
            processData: false,
            cache: false,
            success: function (result) {
                callback();
            },
            error: function (result) {
                callbackFail();
            }
        })
    }

    function getCrontabParameters(callback) {
        $.get("/CrontabParameters/GetCrontabParameters", {}, function (data) {
            console.log(data)
            callback(data);
        }).fail(function () {
            alert('fail')
        })
    }

    function renderCrontabParameters(data) {
        html = "";

        $.each(data, function (index, el) {
            html += '<tr cid="' + el.Id + '">'

            html += '<td>' + el.Name + '</td>'

            html += '<td>' + el.Interval.Minutes + '</td>'
            html += '<td>' + el.InputFolderName + '</td>'
            html += '<td>' + el.ErrorFolderName + '</td>'
            html += '<td>' + el.RootPath + '</td>'
            html += '<td>' + el.OutputFolderName + '</td>'           
            html += '<td>' + (el.IsActive ? 'Yes':'No') + '</td>'

            html += '</tr>'
        })

        $('#crontabParametersContainer tbody').html(html);
        tableClick();
    }

    getCrontabParameters(renderCrontabParameters);
})