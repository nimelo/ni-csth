﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Models
{
    public class Worker
    {
        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();

        public IntPtr GetFW()
        {
            return GetForegroundWindow();
        }

        #region Fields
        private IntPtr lastHwnd;
        private DateTime lastTime;
        private AppInformation lastInfo;
        private bool canLog;
        #endregion


        #region Properties
        private static uint[] systemPids
        {
            get
            {
                return new uint[] { 0, 4, 8 };
            }
        }

        #endregion

        #region Ctors

        public Worker()
        {
            canLog = false;
        }

        #endregion

        #region Methods

        public void LogLine(string toLog)
        {
            System.IO.File.AppendAllText(Path.Combine(ATL.Config.LoggingPath, string.Format(@"{0}{1}", DateTime.Today.ToShortDateString(), ".log")), toLog + Environment.NewLine);
        }

        public void Log(string toLog)
        {
            System.IO.File.AppendAllText(Path.Combine(ATL.Config.LoggingPath, string.Format(@"{0}{1}", DateTime.Today.ToShortDateString(), ".log")), toLog);
        }

        public void DoWork()
        {
            IntPtr currenthWnd = GetFW();
            uint pid;
            AppInformation.GetWindowThreadProcessId(currenthWnd, out pid);
            PerformATLAlgorithm(currenthWnd, pid);

        }

        public void PerformATLAlgorithm(IntPtr currenthWnd, uint pid)
        {
            if (Idle.GetIdleTime() > ATL.Config.IdleInterval)
            {
                if (canLog)
                {
                    Console.WriteLine("IDLE!");
                    this.LogLastProcess();
                    canLog = false;
                }
            }
            else
            {
                if (!canLog)
                {
                    if (!systemPids.Contains(pid))
                    {
                        Console.WriteLine("LAST==NULL");
                        this.SetNewInformations(currenthWnd);
                        canLog = true;
                    }
                }
                else
                {
                    if (lastHwnd != currenthWnd)
                    {
                        if (!systemPids.Contains(pid))
                        {
                            Console.WriteLine("NEW-WINDOW!");
                            this.LogLastProcess();
                            this.SetNewInformations(currenthWnd);
                            canLog = true;
                        }
                    }
                }
            }
        }

        public void LogLastProcess()
        {
            this.LogLine(this.lastTime.ToString());
            this.LogLine(this.lastInfo.ToString());
            this.LogLine(DateTime.Now.ToString());
        }

        public void SetNewInformations(IntPtr hWnd)
        {
            this.lastTime = DateTime.Now;
            this.lastInfo = AppInformation.ExtractFromWindow(hWnd);
            this.lastHwnd = hWnd;
        }

        #endregion
    }
}
