﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices
{
    /*
    -h
    -loggingpath [path]
    -start
    -stop
    -restart              
        */
    public static class CommandLineParser
    {
        public static void Parse(string toParse)
        {
            switch (toParse)
            {
                case "-start":
                    Model.Model.GetInstance.Start();
                    break;
                case "-stop":
                    Model.Model.GetInstance.Stop();
                    break;
                case "-restart":
                    Model.Model.GetInstance.Restart();
                    break;
                case "-exit":
                    Environment.Exit(0);
                    break;
                    
                default:
                    Model.Model.GetInstance.RunCrontabByName(toParse);
                    break;
            }

            Console.WriteLine(toParse);
        }
    }
}
