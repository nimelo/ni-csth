﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ATL.Models
{
    public class PluginModel
    {
        static PluginModel _instance;
        #region Threads
        BackgroundWorker BackgroundWorker { get; set; }
        #endregion

        #region Properties

        public static PluginModel GetInstance
        {
            get
            {
                return _instance ?? (_instance = new PluginModel());
            }
        }
        #endregion

        #region Ctors
        private PluginModel()
        {
            this.BackgroundWorker = new BackgroundWorker();
            this.BackgroundWorker.WorkerSupportsCancellation = true;
            this.BackgroundWorker.DoWork += this.DoWork;
        }

        #endregion

        #region Plugin Methods

        public void Restart()
        {
            this.Stop();
            this.Start();
        }

        public void Start()
        {
            ATL.Initialize();
            if (!this.BackgroundWorker.IsBusy)
                this.BackgroundWorker.RunWorkerAsync();
        }

        public void Stop()
        {
            if (this.BackgroundWorker.IsBusy)
                this.BackgroundWorker.CancelAsync();
        }

        private void DoWork(object s, DoWorkEventArgs e)
        {
            var bw = s as BackgroundWorker;
            var worker = new Worker();
            while (true)
            {
                //TODO add ticks
                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                try
                {
                    worker.DoWork();
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.Message);
                }

                Thread.Sleep(400);
            }
        }
        #endregion
    }
}
