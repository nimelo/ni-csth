﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class SpectrumsEditController : Controller
    {
        // GET: SpectrumsEdit
        public ActionResult Index()
        {
            return View("SpectrumsEditHomepartial");
        }

        [HttpPost]
        public ActionResult AddCategory(string name, bool isRenderable, byte type)
        {
            using (var db = new DbModelContainer())
            {
                if (db.SpectrumCategorySet.Any(x => x.Name == name && x.Type == type))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.SpectrumCategorySet.Add(new SpectrumCategory()
                    {
                        Name = name,
                        Type = type,
                        IsRenderable = isRenderable
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }

            }

            return Json(string.Empty);
        }

        public ActionResult GetCategories()
        {
            List<SpectrumCategory> categories = new List<SpectrumCategory>();
            using (var db = new DbModelContainer())
            {
                db.SpectrumCategorySet.ToList().ForEach(x => categories.Add(new SpectrumCategory()
                {
                    Name = x.Name,
                    IsRenderable = x.IsRenderable,
                    Type = x.Type,
                    Id = x.Id,
                }));
            }

            return Json(categories, JsonRequestBehavior.AllowGet);
        }       

        public ActionResult GetCategoryItemsByCategoryId(int categoryId)
        {
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            using (var db = new DbModelContainer())
            {
                db.CategoryItemSet.Where(x => x.SpectrumCategoryId == categoryId).ToList().ForEach(x => categoryItems.Add(new CategoryItem() { UniqueIdentificator = x.UniqueIdentificator, CategoryId = x.SpectrumCategoryId, Id = x.Id, Type = x.Type }));
            }

            return Json(categoryItems, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditCategory(int categoryId, string name, bool isRenderable, byte type)
        {
            using (var db = new DbModelContainer())
            {
                if (db.SpectrumCategorySet.Any(x => x.Name == name && x.Type == type && x.Id != categoryId) || !db.SpectrumCategorySet.Any(x => x.Id == categoryId))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    var category = db.SpectrumCategorySet.Where(x => x.Id == categoryId).First();
                    category.Name = name;
                    category.IsRenderable = isRenderable;
                    category.Type = type;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }

            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult AddCategoryItem(int categoryId, byte itemType, string identifier)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategoryItemSet.Any(x => x.SpectrumCategoryId == categoryId && x.UniqueIdentificator == identifier && x.Type == itemType))
                {
                    Response.StatusCode = 402;
                }
                else
                {
                    db.CategoryItemSet.Add(new CategoryItem()
                    {
                        SpectrumCategoryId = categoryId,
                        UniqueIdentificator = identifier,
                        Type = itemType
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }
            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult DeleteCategoryItem(int categoryId, int id)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CategoryItemSet.Any(x => x.SpectrumCategoryId == categoryId && x.Id == id))
                {
                    try
                    {
                        db.CategoryItemSet
                        .Remove(db.CategoryItemSet
                                    .Where(x => x.SpectrumCategoryId == categoryId && x.Id == id)
                                    .First());
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 303;
                    }
                }
                else
                {
                    Response.StatusCode = 303;
                }
            }

            return Json(string.Empty);
        }

        [HttpPost]
        public ActionResult DeleteCategory(int categoryId)
        {
            using (var db = new DbModelContainer())
            {
                if (db.SpectrumCategorySet.Any(x => x.Id == categoryId))
                {

                    try
                    {
                        db.CategoryItemSet
                        .RemoveRange(db.CategoryItemSet
                                    .Where(x => x.SpectrumCategoryId == categoryId));
                        db.SpectrumCategorySet.Remove(db.SpectrumCategorySet.Where(x => x.Id == categoryId).First());
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 303;
                    }
                }
                else
                {
                    Response.StatusCode = 303;
                }
            }

            return Json(string.Empty);
        }
    }
}