﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices.Model.ImportCrontabs
{
    public class UsersImportRecord : AbstractRecord
    {
        [Record(Index = 0, MaxLength = 100, Type = RecordTypes.String)]
        public string Login { get; set; }
        [Record(Index = 1, MaxLength = 100, Type = RecordTypes.String)]
        public string FirstName { get; set; }
        [Record(Index = 2, MaxLength = 100, Type = RecordTypes.String)]
        public string LastName { get; set; }
        [Record(Index = 3, MaxLength = 100, Type = RecordTypes.String)]
        public string Password { get; set; }
        [Record(Index = 4, MaxLength = 5, Type = RecordTypes.Bool)]
        public bool IsActive { get; set; }

        public UsersImportRecord()
        {

        }
    }

    public class FellPresenceImportRecord : AbstractRecord
    {
        [Record(Index = 0, MaxLength = 100, Type = RecordTypes.String)]
        public string Login { get; set; }
        [Record(Index = 1, MaxLength = 100, Type = RecordTypes.String)]
        public string FirstName { get; set; }
        [Record(Index = 2, MaxLength = 100, Type = RecordTypes.String)]
        public string LastName { get; set; }
        [Record(Index = 3, MaxLength = 100, Type = RecordTypes.DateTime)]
        public DateTime From { get; set; }
        [Record(Index = 4, MaxLength = 100, Type = RecordTypes.DateTime)]
        public DateTime To { get; set; }

        public FellPresenceImportRecord()
        {

        }
    }

    public class PresenceImportRecord : AbstractRecord
    {
        [Record(Index = 0, MaxLength = 100, Type = RecordTypes.String)]
        public string Login { get; set; }
        [Record(Index = 1, MaxLength = 100, Type = RecordTypes.String)]
        public string FirstName { get; set; }
        [Record(Index = 2, MaxLength = 100, Type = RecordTypes.String)]
        public string LastName { get; set; }
        [Record(Index = 3, MaxLength = 100, Type = RecordTypes.DateTime)]
        public DateTime From { get; set; }
        [Record(Index = 4, MaxLength = 100, Type = RecordTypes.DateTime)]
        public DateTime To { get; set; }
        [Record(Index = 5, MaxLength = 5, Type = RecordTypes.Bool)]
        public bool ExistAppHistories { get; set; }
        [Record(Index = 6, MaxLength = 5, Type = RecordTypes.Bool)]
        public bool ExistWebHistories { get; set; }
        public PresenceImportRecord()
        {

        }
    }

}
