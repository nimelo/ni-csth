﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Communications
{
    public class FTPHelper
    {
        public static void UploadFile(string path, string newFileName)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Credentials = new NetworkCredential("geek", "geek");
                    //client.UseDefaultCredentials = true;
                    client.UploadFile((MA.Config.FTPAddress + newFileName).Replace("\\", "/"),"STOR", path);
                }
                catch (Exception e)
                {
                    throw;
                }

            }
        }
    }
}
