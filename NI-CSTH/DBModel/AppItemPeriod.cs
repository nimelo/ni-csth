//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppItemPeriod
    {
        public int Id { get; set; }
        public System.DateTime From { get; set; }
        public System.DateTime To { get; set; }
        public System.TimeSpan TimeSpan { get; set; }
        public int AppHistoryDomainId { get; set; }
    
        public virtual AppHistory AppHistory { get; set; }
        public virtual AppHistoryDomain AppHistoryDomain { get; set; }
    }
}
