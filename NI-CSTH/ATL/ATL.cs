﻿using ATL.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL
{
    public class ATL
    {
        #region Fields

        private static ATLConfiguration _config;

        #endregion
        #region Properties

        public static ATLConfiguration Config
        {
            get
            {
                if (_config == null)
                {
                    return ConfigurationManager.GetSection(typeof(ATLConfiguration).Name) as ATLConfiguration;
                }
                else
                    return _config;
            }
        }

        #endregion

        #region Methods

        public static void Initialize()
        {
            var config = ConfigurationManager.GetSection(typeof(ATLConfiguration).Name) as ATLConfiguration;
            if (config == null)
            {
                config = ATLConfiguration.Default;
                config.Save();
                _config = config;
            }
        }

        public static void StartPipeServer()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    using (var server = new NamedPipeServerStream("ATL"))
                    {
                        server.WaitForConnection();
                        using (var reader = new StreamReader(server))
                        {
                            var line = reader.ReadLine();
                            if (line != null)
                                CommandLineParser.Parse(line);
                        }
                    }
                }
            });
        }

        public static void NotifyPipeServer(string toNotify)
        {
            using (var client = new NamedPipeClientStream("ATL"))
            {
                client.Connect();
                using (StreamWriter writer = new StreamWriter(client))
                {

                    writer.WriteLine(toNotify);
                    writer.Flush();
                }
            }
        }

        #endregion
    }
}
