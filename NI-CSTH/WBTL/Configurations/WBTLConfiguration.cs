﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WBTL.Configurations
{
    public class WBTLConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("LoggingPath")]
        public string LoggingPath
        {
            get { return (string)this["LoggingPath"]; }
            set { this["LoggingPath"] = value; }
        }

        [ConfigurationProperty("WSSPort")]
        public int WSSPort
        {
            get { return (int)this["WSSPort"]; }
            set { this["WSSPort"] = value; }
        }

        [ConfigurationProperty("WSSName")]
        public string WSSName
        {
            get { return (string)this["WSSName"]; }
            set { this["WSSName"] = value; }
        }

        public static WBTLConfiguration Default
        {
            get
            {
                return new WBTLConfiguration()
                {
                    LoggingPath = Environment.CurrentDirectory,
                    WSSName = @"FileHistory",
                    WSSPort = 4649
                };
            }
        }

        public WBTLConfiguration()
        {

        }

        public override bool IsReadOnly()
        {
            return false;
        }

        public void Save()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(null);

            WBTLConfiguration instance = (WBTLConfiguration)config.GetSection(this.GetType().Name);

            if (instance == null)
            {
                config.Sections.Add(this.GetType().Name, WBTLConfiguration.Default);
                WBTLConfiguration.Default.SectionInformation.ForceSave = true;
            }
            else
            {
                instance.LoggingPath = this.LoggingPath;
            }

            this.CreateDirectory((WBTLConfiguration)config.GetSection(this.GetType().Name));

            config.Save(ConfigurationSaveMode.Full);
        }

        private void CreateDirectory(WBTLConfiguration instance)
        {
            if (!Directory.Exists(instance.LoggingPath))
            {
                Directory.CreateDirectory(instance.LoggingPath);
            }
        }
    }
}
