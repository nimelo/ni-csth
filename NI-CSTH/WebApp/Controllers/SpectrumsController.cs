﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class SpectrumsController : Controller
    {
        // GET: Spectrums
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetChartDataForSpectrum(int spectrumId, string login, bool forAll)
        {
            Dictionary<string, Dictionary<string, TimeSpan>> spectrum = new Dictionary<string, Dictionary<string, TimeSpan>>();
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            Dictionary<string, string> mapping = new Dictionary<string, string>();
            byte spectrumType;

            using (var db = new DbModelContainer())
            {
                spectrumType = db.SpectrumCategorySet.Where(x => x.Id == spectrumId).First().Type;
            }
            List<string> logins = new List<string>();

            using (var db = new DbModelContainer())
            {
                logins = db.UserSet.Select(x => x.Login).ToList();
                categoryItems = db.CategoryItemSet.Where(x => x.SpectrumCategoryId == spectrumId).ToList();
                mapping = db.AppPathMapSet.ToDictionary(x => x.Path, x => x.Name);
            }

            GetSpectrumIntervalsByType(spectrumType, DateTime.Today).ForEach(x =>
            {
                spectrum.Add(x, new Dictionary<string, TimeSpan>());
                categoryItems.ForEach(y => spectrum[x].Add(y.UniqueIdentificator, new TimeSpan()));
            });


            if (forAll)
            {
                logins.ForEach(x =>
                {
                    var tmpSpectrum = GetDataForSpectrum(x, spectrumId, spectrumType);

                    tmpSpectrum.ToList().ForEach(y =>
                    {
                        tmpSpectrum[y.Key].ToList().ForEach(z =>
                        {
                            spectrum[y.Key][z.Key] += z.Value;
                        });
                    });
                });
            }
            else
            {
                spectrum = GetDataForSpectrum(login, spectrumId, spectrumType);
            }

            var intervals = GetSpectrumIntervalsByType(spectrumType, DateTime.Today);

            Dictionary<string, List<TimeSpan>> series = new Dictionary<string, List<TimeSpan>>();
            List<object> returnList = new List<object>();
            categoryItems.ForEach(x =>
            {
                if (mapping.Keys.Contains(x.UniqueIdentificator))
                {
                    series.Add(mapping[x.UniqueIdentificator], new List<TimeSpan>());
                }
                else
                {
                    series.Add(x.UniqueIdentificator, new List<TimeSpan>());
                }                   
            });

            spectrum.ToList().ForEach(x =>
                    {
                        x.Value.ToList().ForEach(y =>
                        {
                            if (mapping.Keys.Contains(y.Key))
                            {
                                series[mapping[y.Key]].Add(y.Value);
                            }
                            else
                            {
                                series[y.Key].Add(y.Value);
                            }
                        });
                    }
            );

            series.ToList().ForEach(x =>
            {
                returnList.Add(new
                {
                    name = x.Key,
                    data = x.Value.Select(z => new { y = Math.Round(z.TotalHours, 3) })
                });
            });

            return Json(new { categories = intervals, data = returnList }, JsonRequestBehavior.AllowGet);
        }


        private Dictionary<string, Dictionary<string, TimeSpan>> GetDataForSpectrum(string login, int spectrumId, byte spectrumType)
        {
            DateTime fromDate = DateTime.MinValue;
            switch (spectrumType)
            {
                case 0:
                    fromDate = DateTime.Today.AddDays(-6);
                    break;
                case 1:
                    fromDate = DateTime.Today.AddDays(-30);
                    break;
                case 2:
                    fromDate = DateTime.Today.AddMonths(-11);
                    break;
            }

            DateTime toDate = DateTime.Today;//DateTime.Today.AddDays(-1);

            List<string> intervals = GetSpectrumIntervalsByType(spectrumType, DateTime.Today);

            Dictionary<string, Dictionary<string, TimeSpan>> spectrum = new Dictionary<string, Dictionary<string, TimeSpan>>();

            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            List<CategoryItem> categoryItems = new List<CategoryItem>();
            List<History> histories = new List<History>();

            int iterator = 0;
            if (spectrumType == 0 || spectrumType == 1)
            {
                for (DateTime i = fromDate; i <= toDate; i = i.AddDays(1))
                {
                    GetHistoriesBy(login, spectrumId, i, i, out categoryItems, out histories);
                    spectrum.Add(intervals[iterator], ProcessHistory(categoryItems, histories));
                    iterator++;
                }
            }
            else if (spectrumType == 2)
            {
                for (DateTime i = fromDate; i <= toDate; i = i.AddMonths(1))
                {
                    var firstDayOfMonth = new DateTime(i.Year, i.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    GetHistoriesBy(login, spectrumId, firstDayOfMonth, lastDayOfMonth, out categoryItems, out histories);
                    spectrum.Add(intervals[iterator], ProcessHistory(categoryItems, histories));
                    iterator++;
                }
            }

            return spectrum;
        }

        private void GetHistoriesBy(string login, int spectrumId, DateTime fromDate, DateTime toDate, out List<CategoryItem> categoryItems, out List<History> histories)
        {
            using (var db = new DbModelContainer())
            {
                db.Configuration.LazyLoadingEnabled = false;

                histories = db.HistorySet
                    .Include("WebHistoryDomain")
                    .Include("AppHistoryDomain")
                    .Where(h => h.Day >= fromDate && h.Day <= toDate && h.Account.User.Login == login).ToList();
                categoryItems = db.CategoryItemSet.Where(x => x.SpectrumCategoryId == spectrumId).ToList();
            }
        }

        private Dictionary<string, TimeSpan> ProcessHistory(List<CategoryItem> categoryItems, List<History> histories)
        {
            Dictionary<string, TimeSpan> dic = new Dictionary<string, TimeSpan>();
            categoryItems.ForEach(x => dic.Add(x.UniqueIdentificator, new TimeSpan()));

            categoryItems.ForEach(ci =>
            {
                if (ci.Type == 0)
                {
                    histories.Where(h => h.ExistAppHistories)
                                .SelectMany(whd => whd.AppHistoryDomain
                                                        .Where(d => d.Path == ci.UniqueIdentificator)).ToList()
                                                        .ForEach(x =>
                                                        {
                                                            dic[x.Path] += x.SumTimeSpan;
                                                        });
                }
                else if (ci.Type == 1)
                {
                    histories.Where(h => h.ExistWebHistories)
                                 .SelectMany(whd => whd.WebHistoryDomain
                                                         .Where(d => d.Domain == ci.UniqueIdentificator)).ToList()
                                                         .ForEach(x =>
                                                         {
                                                             dic[x.Domain] += x.SumTimeSpan;
                                                         });
                }
            });

            return dic;
        }

        public ActionResult GetRenderableSpectrums()
        {
            List<SpectrumCategory> categories = new List<SpectrumCategory>();
            using (var db = new DbModelContainer())
            {
                db.SpectrumCategorySet.Where(z => z.IsRenderable).ToList().ForEach(x => categories.Add(new SpectrumCategory()
                {
                    Name = x.Name,
                    IsRenderable = x.IsRenderable,
                    Type = x.Type,
                    Id = x.Id,
                }));
            }

            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        //TODO
        private List<string> GetSpectrumIntervalsByType(byte type, DateTime to)
        {
            List<string> intervals = new List<string>();

            string[] months = new[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            switch (type)
            {
                case 1:
                    for (int i = 31
                        ; i > 0; i--)
                    {
                        intervals.Add(to.AddDays(-(i - 1)).ToShortDateString());
                    }
                    break;
                case 2:

                    for (int i = 12; i > 0; i--)
                    {
                        intervals.Add(months[to.AddMonths(-(i - 1)).Month - 1]);
                    }
                    break;
                case 0:

                    for (int i = 7; i > 0; i--)
                    {
                        intervals.Add(to.AddDays(-(i - 1)).DayOfWeek.ToString());
                    }

                    break;
                default:
                    break;
            }

            return intervals;
        }
    }
}