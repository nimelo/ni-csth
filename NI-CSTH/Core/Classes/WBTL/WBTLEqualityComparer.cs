﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Classes.WBTL
{
    class WBTLEqualityComparer : IEqualityComparer<WBTLEntry>
    {
        public bool Equals(WBTLEntry x, WBTLEntry y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(WBTLEntry obj)
        {
            return 0;
        }
    }
}
