﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ManagmentApplication.Converters
{
    class BoolToVisibilityConverter : IValueConverter
    {
        public bool IsInverted { get; set; }

        public BoolToVisibilityConverter()
        {
            this.IsInverted = false;
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.IsInverted)
            {
                return (bool)value == true ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return (bool)value == true ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
