﻿$(function () {

    var exampleData = [
        {
            name: 'top1',
            color: '#FF0220',
            data: [{
                name: "Microsoft Internet Explorer",
                y: 56.33
            }, {
                name: "Chrome",
                y: 24.03,
            }, {
                name: "Firefox",
                y: 10.38
            }, {
                name: "Safari",
                y: 4.77
            }, {
                name: "Opera",
                y: 0.91
            }, {
                name: "Proprietary or Undetectable",
                y: 0.2
            }]
        }]
    ;

    function getRenderableSpectrums() {
        $.get("/Spectrums/GetRenderableSpectrums", {}, function (data) {

            $.each(data, function (index, el) {
                addSpectrum(el.Name, el.Id);
                getSpectrumData(el.Id);
            })

        }).fail(function () {
            alert('fail')
        })
    }

    function getSpectrumData(id) {
        $.get("/Spectrums/GetChartDataForSpectrum", { spectrumId: id, from: "", to: "", login: "", forAll: true }, function (data) {
            console.log(data)
            ChartsRendering.renderColumn('#spectrum' + id, data.data, data.categories);
        }).fail(function () {
            alert('fail')
        })
    }

    function addSpectrum(name, id) {
        html = ""
        html += '<div class="left half">'
        html += '<h2 class="text-center">' + name + '</h2>'
        html += ' <div id="spectrum' + id + '" class="basic-chart"></div></div>'

        $('#charts').append(html);
    }

    getRenderableSpectrums();

    var exampleCategories = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];



  //  ChartsRendering.renderColumn('#webStatistics', exampleData, exampleCategories);
  //  ChartsRendering.renderColumn('#appStatistics', exampleData, exampleCategories);
  //  ChartsRendering.renderColumn('#idleStatistics', exampleData, exampleCategories);

});