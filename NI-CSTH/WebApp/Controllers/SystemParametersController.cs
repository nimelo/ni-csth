﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class SystemParametersController : Controller
    {
        // GET: SystemParameters
        public ActionResult Index()
        {
            return View("SystemParametersHomePartial");
        }

        [HttpPost]
        public ActionResult ModifySystemParameter(string key, string value)
        {
            using (var db = new DbModelContainer())
            {
                if (db.SystemParameterSet.Any(x => x.Key == key))
                {
                    db.SystemParameterSet.Where(x => x.Key == key).First().Value = value;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }
                else
                {
                    Response.StatusCode = 402;
                }
            }

            return Json(string.Empty);
        }

        public ActionResult GetSystemParameters()
        {
            List<SystemParameter> parameters = new List<SystemParameter>();
            using (var db = new DbModelContainer())
            {
                db.SystemParameterSet.ToList().ForEach(x => parameters.Add(new SystemParameter()
                {
                    Key = x.Key,
                    Name = x.Name,
                    Value = x.Value,
                    Id = x.Id
                }));
            }

            return Json(parameters, JsonRequestBehavior.AllowGet);
        }
    }
}