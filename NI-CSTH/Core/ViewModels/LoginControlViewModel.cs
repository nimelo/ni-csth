﻿using Common;
using ManagmentApplication.WCFControlServices.ConfigurationService;
using ManagmentApplication.WCFControlServices.LoginService;
using ManagmentApplication.WCFControlServices.ScheduleService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ManagmentApplication.ViewModels
{
    public class LoginControlViewModel : BaseViewModel
    {
        #region Fields
        private string _login;
        private bool _isLogged;
        private string _password;
        #endregion

        #region Properties
        public bool IsLogged
        {
            get { return _isLogged; }
            set
            {
                _isLogged = value;
                this.NotifyPropertyChanged();
            }
        }

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                this.NotifyPropertyChanged();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                this.NotifyPropertyChanged();
            }
        }
        #endregion

        #region Ctors
        public LoginControlViewModel()
        {
            this.Login = string.Empty;
            this.Password = string.Empty;
            this.IsLogged = false;
        }
        #endregion

        #region Commands
        private ICommand _loginCommand;

        public ICommand LoginCommand
        {
            get { return _loginCommand ?? (_loginCommand = new RelayCommand(x => this.LoginAction())); }
        }

        #endregion

        #region Methods
        private async void LoginAction()
        {
            using (var service = new LoginServiceClient())
            {
                this.IsLogged = await service.LogAsync(this.Login, this.Password);
            }
            if (this.IsLogged)
                PostLoginAction();

        }

        private void PostLoginAction()
        {
            MA.Login = this.Login;
            MA.Password = this.Password;

            using (var service = new ConfigurationServiceClient())
            {
                //TODO Configuration
            }

            using (var service = new ScheduleServiceClient())
            {
                //TODO Schedule
                Schedulers.LogCollectorScheduler.Start(service.GetSchedule(this.Login));               
            }
        }
        #endregion
    }
}
