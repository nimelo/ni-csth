﻿namespace CrontabServices.Model
{
    public enum RecordTypes
    {
        DateTime,
        String,
        Int,
        Double,
        Bool
    }
}