﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class AppPathMapController : Controller
    {
        // GET: AppPathMap
        public ActionResult Index()
        {
            return View("AppPathMapPartial");
        }

        public ActionResult Get()
        {
            List<AppPathMap> list = new List<AppPathMap>();
            using (var db = new DbModelContainer())
            {
                db.AppPathMapSet.ToList().ForEach(x =>
                {
                    list.Add(new AppPathMap()
                    {
                       Name = x.Name,
                       Path = x.Path,
                       Id = x.Id
                    });
                });
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var db = new DbModelContainer())
            {
                if(db.AppPathMapSet.Any(x=> x.Id == id))
                {
                    db.AppPathMapSet.Remove(db.AppPathMapSet.Where(x => x.Id == id).First());
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {

                        Response.StatusCode = 402;
                    }                   
                }
                else
                {
                    Response.StatusCode = 402;
                }
            }
            return Json("");
        }

        [HttpPost]
        public ActionResult Add(string path, string name)
        {
            using (var db = new DbModelContainer())
            {
                if (!db.AppPathMapSet.Any(x => x.Path == path && x.Name == name))
                {
                    db.AppPathMapSet.Add(new AppPathMap()
                    {
                        Name = name,
                        Path = path
                    });

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {

                        Response.StatusCode = 402;
                    }
                }
                else
                {
                    Response.StatusCode = 402;
                }
            }
            return Json("");
        }
    }
}