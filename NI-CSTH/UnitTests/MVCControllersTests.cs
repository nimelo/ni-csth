﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ATL.Models;
using Moq;
using CrontabServices.Model;
using WebApp.Controllers;

namespace UnitTests
{
    [TestClass]
    public class MVCControllersTests
    {
        [TestMethod]
        public void MyTestMethod()
        {
            Mock<CategoriesController> cc = new Mock<CategoriesController>();
            cc.CallBase = true;
            var obj = cc.Object.GetCategories();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.Category>)((System.Web.Mvc.JsonResult)obj).Data).Count);

        }

        [TestMethod]
        public void MyTestMethod2()
        {
            Mock<CategoriesController> cc = new Mock<CategoriesController>();
            cc.CallBase = true;
            var obj = cc.Object.GetRenderableCategories();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.Category>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }

        [TestMethod]
        public void MyTestMethod3()
        {
            Mock<AppPathMapController> cc = new Mock<AppPathMapController>();
            cc.CallBase = true;
            var obj = cc.Object.Get();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.AppPathMap>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }

        [TestMethod]
        public void MyTestMethod4()
        {
            Mock<CrontabsController> cc = new Mock<CrontabsController>();
            cc.CallBase = true;
            var obj = cc.Object.GetCrontabStatuses();

            Assert.AreEqual(0, ((System.Collections.Generic.List<object>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }

        [TestMethod]
        public void MyTestMethod5()
        {
            Mock<SpectrumsController> cc = new Mock<SpectrumsController>();
            cc.CallBase = true;
            var obj = cc.Object.GetRenderableSpectrums();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.SpectrumCategory>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }

        [TestMethod]
        public void MyTestMethod6()
        {
            Mock<SpectrumsEditController> cc = new Mock<SpectrumsEditController>();
            cc.CallBase = true;
            var obj = cc.Object.GetCategories();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.SpectrumCategory>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }

        [TestMethod]
        public void MyTestMethod7()
        {
            Mock<SystemParametersController> cc = new Mock<SystemParametersController>();
            cc.CallBase = true;
            var obj = cc.Object.GetSystemParameters();

            Assert.AreEqual(0, ((System.Collections.Generic.List<DBModel.SystemParameter>)((System.Web.Mvc.JsonResult)obj).Data).Count);
        }
    }
}