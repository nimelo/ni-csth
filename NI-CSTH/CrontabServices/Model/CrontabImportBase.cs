﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBModel;
using System.IO;

namespace CrontabServices.Model
{
    public abstract class CrontabImportBase<T> : CrontabBase where T : AbstractRecord
    {
        public CrontabImportBase(Crontab crontab) : base(crontab)
        {
        }

        public virtual List<string> GetFileNames()
        {
            List<string> names = new List<string>();

            names = Directory.GetFiles(Path.Combine(this.Crontab.RootPath, this.Crontab.InputFolderName)).ToList();

            return names;
        }

        public override void DoWork()
        {
            Before();

            var fileNames = this.GetFileNames();

            fileNames.ForEach(x =>
            {
                try
                {
                    this.Process(x);
                }
                catch (InvalidFileExcepction e)
                {
                    File.Move(x, Path.Combine(this.Crontab.RootPath, this.Crontab.ErrorFolderName, Path.GetFileName(x)));
                    Console.WriteLine("Invalid file: {0}", x);
                }

            });
        }

        private void Before()
        {
            var inPath = Path.Combine(this.Crontab.RootPath, this.Crontab.InputFolderName);
            var errorPath = Path.Combine(this.Crontab.RootPath, this.Crontab.ErrorFolderName);
            var outPath = Path.Combine(this.Crontab.RootPath, this.Crontab.OutputFolderName);

            this.CreateDirectories(inPath, errorPath, outPath);
        }

        private void CreateDirectories(string inPath, string errorPath, string outPath)
        {
            if (!Directory.Exists(inPath))
            {
                Directory.CreateDirectory(inPath);
            }

            if (!Directory.Exists(errorPath))
            {
                Directory.CreateDirectory(errorPath);
            }

            if (!Directory.Exists(outPath))
            {
                Directory.CreateDirectory(outPath);
            }
        }

        protected void Process(string x)
        {
            var file = File.ReadAllLines(x);

            if (file.Count() < 2)
            {
                Console.WriteLine("Error in file: {0}", x);
                throw new InvalidFileExcepction();
            }
            else
            {
                try
                {
                    var amountOfRecords = this.ProcessHeader(file[0], Path.GetFileName(x));
                    var wrongRecords = this.ProcessRecords(this.SubArray(file, 1, file.Length - 1), amountOfRecords);

                    this.WriteErrorRecords(wrongRecords, Path.GetFileName(x));
                    File.Move(x, Path.Combine(this.Crontab.RootPath, this.Crontab.OutputFolderName, Path.GetFileName(x)));
                }
                catch (InvalidHeaderExcepction e)
                {
                    File.Move(x, Path.Combine(this.Crontab.RootPath, this.Crontab.ErrorFolderName, Path.GetFileName(x)));
                    Console.WriteLine("Invalid header in file: {0}", x);
                }
            }
        }

        protected virtual void WriteErrorRecords(List<Tuple<string, string>> wrongRecords, string fileName)
        {
            var filePath = Path.Combine(this.Crontab.RootPath, this.Crontab.ErrorFolderName, fileName);

            var fileContent = string.Empty;

            wrongRecords.ForEach(x =>
            {
                fileContent += x.Item1 + "|" + x.Item2 + Environment.NewLine;
            });

            File.WriteAllText(filePath, fileContent);
        }

        private List<Tuple<string, string>> ProcessRecords(IEnumerable<string> records, int amountOfRecords)
        {
            if (records.Count() != amountOfRecords)
            {
                throw new AmountOfRecordsNotCorrectExcepction();
            }

            List<Tuple<string, string>> wrongRecords = new List<Tuple<string, string>>();

            records.ToList().ForEach(x =>
            {
                try
                {
                    var newRecord = this.ProcessRecord(x);
                    this.SaveToDb(newRecord);
                }
                catch (InvalidRecordFieldExcepction irfe)
                {
                    wrongRecords.Add(new Tuple<string, string>(x, irfe.ToString()));
                }
                catch (InvalidRecordProperitesCountException irpc)
                {
                    wrongRecords.Add(new Tuple<string, string>(x, irpc.ToString()));
                }
                catch (Exception e)
                {
                    wrongRecords.Add(new Tuple<string, string>(x, "DBERROR"));
                }

            });

            return wrongRecords;
        }

        public virtual T ProcessRecord(string record)
        {
            var splittedRecords = record.Split(new char[] { '|' });

            var newRecord = RecordAttribute.Fill<T>(splittedRecords);

            return newRecord;
        }

        protected abstract void SaveToDb(T record);

        public int ProcessHeader(string header, string fileName)
        {
            var splittedHeader = header.Split(new char[] { '|' });

            if (fileName != splittedHeader[0])
                throw new InvalidHeaderExcepction();

            int amountOfRecords = 0;
            try
            {
                amountOfRecords = Convert.ToInt32(splittedHeader[1]);
                return amountOfRecords;
            }
            catch (Exception e)
            {
                throw new InvalidHeaderExcepction();
            }
        }

        public virtual A[] SubArray<A>(A[] data, int index, int length)
        {
            A[] result = new A[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}
