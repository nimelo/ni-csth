﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFControlServices.DataContracts;
using WCFControlServices.Interfaces;
using DBModel;

namespace WCFControlServices.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ScheduleService" in both code and config file together.
    public class ScheduleService : IScheduleService
    {
        public UserSchedule GetSchedule(string login)
        {
            var schedule = new UserSchedule();

            List<History> historyList = new List<History>();

            using (var db = new DBModel.DbModelContainer())
            {
                try
                {
                    historyList = db.UserSet
                          .Include(u => u.Account)
                          .Include(a => a.Account.History)
                          .Where(x => x.Login == login).First()?.Account.History.Where(hist => hist.ExistWebFtp == false || hist.ExistAppFtp == false).ToList();
                }
                catch (Exception e)
                {
                    //TODONotification
                }
            }

            foreach (var day in historyList)
            {
                if (!day.ExistAppHistories)
                    schedule.DesktopLogList.Add(day.Day);

                if (!day.ExistWebHistories)
                    schedule.WebLogList.Add(day.Day);
            }

            return schedule;
        }

        public void Uploaded(string login, DateTime day, bool app, bool web)
        {
            using (var db = new DBModel.DbModelContainer())
            {
                try
                {
                     var history = db.HistorySet.Where(x => x.Day == day && x.Account.User.Login == login).First();

                    if(app)
                    {
                        history.ExistAppFtp = true;
                    }

                    if (web)
                    {
                        history.ExistWebFtp = true;
                    }

                    db.SaveChanges();
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
