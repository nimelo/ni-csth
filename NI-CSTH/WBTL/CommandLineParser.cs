﻿using WBTL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace WBTL
{
    /*
    -h
    -loggingpath [path]
    -start
    -stop
    -restart              
        */
    public static class CommandLineParser
    {
        public static void Parse(string toParse)
        {
            switch (toParse)
            {
                case "-start":
                    PluginModel.GetInstance.Start();
                    Console.WriteLine($"Started WSS at name:{WBTL.Config.WSSName} port:{WBTL.Config.WSSPort}!");
                    break;
                case "-stop":
                    PluginModel.GetInstance.Stop();
                    Console.WriteLine("Server stopped!");
                    break;
                case "-restart":
                    PluginModel.GetInstance.Restart();
                    break;
                case "-logToday":
                    PluginModel.GetInstance.GetLog(DateTime.Today);
                    break;
                case "-exit":
                    Environment.Exit(0);
                    break;
                default:
                    try
                    {
                        var date = DateTime.ParseExact(toParse, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                        PluginModel.GetInstance.GetLog(date);
                    }
                    catch (Exception e)
                    {
                            
                    }
                   
                    break;
            }
            Console.WriteLine(toParse);
        }
    }
}
