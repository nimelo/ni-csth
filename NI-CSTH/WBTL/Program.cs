﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBTL
{
    class Program
    {
        static void Main(string[] args)
        {
            WBTL.Initialize();
            try
            {
                using (new SingleGlobalInstance(2000))
                {
                    Console.WriteLine("Main instance!");
                    WBTL.StartPipeServer();
                    CommandLineParser.Parse("-start");
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Other instance!");
                if (args.Length == 1)
                    WBTL.NotifyPipeServer(args[0]);

                Environment.Exit(0);
            }
        }
    }
}
