﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCFControlServices.DataContracts
{
    [DataContract]
    public class UserSchedule
    {
        [DataMember]
        public List<DateTime> DesktopLogList { get; set; }

        [DataMember]
        public List<DateTime> WebLogList { get; set; }

        public UserSchedule()
        {
            this.DesktopLogList = new List<DateTime>();
            this.WebLogList = new List<DateTime>();
        }
    }
}
