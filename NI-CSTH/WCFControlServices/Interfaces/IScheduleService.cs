﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFControlServices.DataContracts;

namespace WCFControlServices.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IScheduleService" in both code and config file together.
    [ServiceContract]
    public interface IScheduleService
    {
        [OperationContract]
        UserSchedule GetSchedule(string login);
        [OperationContract]
        void Uploaded(string login, DateTime day, bool app, bool web);
    }
}
