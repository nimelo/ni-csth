﻿$(function () {


    function rem(el) {
        el.remove();
    }

    function renderCategoryAdd() {
        var html = "";
        html += '<tr class="edit alert alert-info">'

        html += '<td>' + '<input id="inNameCategory" type="text" class="form-control" style="display: inline-block;"/>' + '</td>'
        html += '<td>' + '<input id="inIsRenderableCategory" type="text" class="form-control" style="display: inline-block;"/>' + '</td>'

        html += '<td>' + '<button class="addCategory btn glyphicon glyphicon-ok-circle"></button>' + '</td>'

        html += '</tr>'
        return html;
    }


    /*Categories*/

    function getCategories(callback) {
        $.get("/AppPathMap/Get", {}, function (data) {
            console.log(data)
            callback(data)
        }).fail(function () {
            alert('fail')
        })
    }

    function renderCategories(list) {
        html = "";
        $.each(list, function (index, el) {
            html += '<tr ids="' + el.Id + '" class="category">'
            html += '<td>' + el.Path + '</td>'
            html += '<td>' + el.Name + '</td>'
            html += '<td><button class="removeCategoryItem glyphicon glyphicon-remove-circle btn"></button></td>'
            html += '</tr>'
        })
        html += renderCategoryAdd();
        $('#categoriesContainer tbody').html(html);
        categoryAddClick();
        categoryItemClick();

    }

    function rowCategoryToJson(row) {
        var json = Object();

        json.name = row.querySelector('td:nth-child(2)').innerHTML
        json.path = row.querySelector('td:nth-child(1)').innerHTML

        return json;
    }

    function deleteCategory(id, callback) {
        $.post("AppPathMap/Delete", { id: id }, function (data) {
            callback();
        }).fail(function () {
            //alert('fail');
        })
    }

    function categoryItemClick() {
        $('.removeCategoryItem').on('click', function (event) {
            var cId = $(this).closest('tr').attr('ids');

            deleteCategory(cId, $(this).closest('tr').remove());
        });
    }

    function categoryAddClick() {
        $('.addCategory').click(function () {

            var tr = $(this).closest('tr');

            $.ajax({
                url: '/AppPathMap/Add',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ path: $('#inNameCategory').val(), name: $('#inIsRenderableCategory').val() }),
                async: true,
                processData: false,
                cache: false,
                success: function (result) {
                    tr.addClass('alert').addClass('alert-success');
                    getCategories(renderCategories);
                },
                error: function (result) {
                    alert('fail')
                    tr.closest('tr').addClass('alert').addClass('alert-danger');
                }
            })
        })
    }
    /*Fakes*/
    var fakeCategoryItems = [
{ identifier: 'facebook.com' }, { identifier: 'facebook.com' }, { identifier: 'facebook.com' }]


    /*Invokes*/

    getCategories(renderCategories);


})