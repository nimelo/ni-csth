﻿using System;
using System.Runtime.Serialization;

namespace CrontabServices.Model
{
    [Serializable]
    internal class InvalidFileExcepction : Exception
    {
        public InvalidFileExcepction()
        {
        }

        public InvalidFileExcepction(string message) : base(message)
        {
        }

        public InvalidFileExcepction(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidFileExcepction(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}