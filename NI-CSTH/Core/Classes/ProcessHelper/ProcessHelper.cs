﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ManagmentApplication.Classes.ProcessHelper
{
    public class ProcessHelper
    {
        public static void StartProgram(string path, string argument)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = argument;
            start.FileName = path;
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            Process.Start(start);
        }

        public static void StartATL()
        {
            Task.Run(() =>
            {
                try
                {
                    ProcessHelper.StartProgram(MA.Config.ATLExecutablePath, "-start");
                    Thread.Sleep(2000);
                    ProcessHelper.StartProgram(MA.Config.ATLExecutablePath, "-start");
                }
                catch (Exception e)
                {
                    //TODO Notification
                }

            });
        }

        //TODO DateTime
        public static void StartWBTL()
        {
            Task.Run(() =>
            {
                try
                {
                    ProcessHelper.StartProgram(MA.Config.WBTLExecutablePath, "-start");
                    Thread.Sleep(2000);
                    ProcessHelper.StartProgram(MA.Config.ATLExecutablePath, "-start");
                }
                catch (Exception e)
                {
                    //TODO Notification
                }

            });
        }

    }
}
