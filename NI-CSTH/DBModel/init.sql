﻿USE NICSTH;

INSERT INTO [dbo].[AccountSet]
           ([FirstName]
           ,[LastName])
     VALUES
           ('Mateusz'
           ,'Gasior')
GO

INSERT INTO UserSet(Login, Password, IsActive, Account_Id) 
VALUES ('root', 'root', 1, 1)

GO

USE [NICSTH]
GO

INSERT INTO [dbo].[CrontabSet]
           ([LastExecution]
           ,[Name]
           ,[Interval]
           ,[InputFolderName]
           ,[ErrorFolderName]
           ,[RootPath]
           ,[OutputFolderName]
           ,[IsActive])
     VALUES
			/*Desktop*/
           (GETDATE ( )
           ,'DesktopLogCrontab'
           ,'00:01:00'
           ,'inApp'
           ,'errApp'
           ,'E:\!FTP'
           ,'outApp'
           ,1),
		   /*Web*/
		   (GETDATE ( )
           ,'WebLogCrontab'
           ,'00:01:00'
           ,'inWeb'
           ,'errWeb'
           ,'E:\!FTP'
           ,'outWeb'
           ,1),
		   /*History*/
		   (GETDATE ( )
           ,'HistoryUpgradeCrontab'
           ,'00:01:00'
           ,''
           ,''
           ,'E:\!FTP'
           ,''
           ,1),
		   /*Migration*/
		   (GETDATE ( )
           ,'UserImportCrontab'
           ,'00:01:00'
           ,'inUserImportCrontab'
           ,'errUserImportCrontab'
           ,'E:\!FTP'
           ,'outUserImportCrontab'
           ,1),
		   (GETDATE ( )
           ,'FellPresenceImportCrontab'
           ,'00:01:00'
           ,'inFellPresenceImportCrontab'
           ,'errFellPresenceImportCrontab'
           ,'E:\!FTP'
           ,'outFellPresenceImportCrontab'
           ,1),
		   (GETDATE ( )
           ,'PresenceImportCrontab'
           ,'00:01:00'
           ,'inPresenceImportCrontab'
           ,'errPresenceImportCrontab'
           ,'E:\!FTP'
           ,'outPresenceImportCrontab'
           ,1)
GO

INSERT INTO [dbo].[SystemParameterSet]
           ([Key]
           ,[Value],
		   [Name])
     VALUES
           ('CrontabSchedulerInterval'
           ,'10',
		   'Crontab Scheduler Interval')
GO


