﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class TrendsController : Controller
    {
        // GET: Trends
        [AllowAnonymous]
        public ActionResult Index()
        {
            return PartialView("TrendsHomePartial");
        }
    }
}