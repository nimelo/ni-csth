﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class CrontabParametersController : Controller
    {
        // GET: CrontabParameters
        public ActionResult Index()
        {
            return View("CrontabParametersHomePartial");
        }

        [HttpPost]
        public ActionResult ModifyCrontabParameter(int crontabId, string interval, string input, string output, string error, string root, bool isActive)
        {
            using (var db = new DbModelContainer())
            {
                if (db.CrontabSet.Any(x => x.Id == crontabId))
                {
                    var crontab = db.CrontabSet.Where(x => x.Id == crontabId).First();
                    
                    try
                    {
                        var timestamp = new TimeSpan(0, int.Parse(interval), 0);
                        crontab.Interval = timestamp;
                        crontab.InputFolderName = input;
                        crontab.OutputFolderName = output;
                        crontab.ErrorFolderName = error;
                        crontab.RootPath = root;
                        crontab.IsActive = isActive;
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Response.StatusCode = 402;
                    }
                }
                else
                {
                    Response.StatusCode = 402;
                }
            }

            return Json(string.Empty);
        }

        public ActionResult GetCrontabParameters()
        {
            List<Crontab> parameters = new List<Crontab>();
            using (var db = new DbModelContainer())
            {
                db.CrontabSet.ToList().ForEach(x => parameters.Add(new Crontab()
                {
                    Name = x.Name,
                    Id = x.Id,
                    LastExecution = x.LastExecution,
                    Interval = x.Interval,
                    InputFolderName = x.InputFolderName,
                    ErrorFolderName = x.ErrorFolderName,
                    OutputFolderName = x.OutputFolderName,
                    RootPath = x.RootPath,
                    IsActive = x.IsActive
                }));
            }

            return Json(parameters, JsonRequestBehavior.AllowGet);
        }
    }
}