﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WBTL.Queue;
using WebSocketSharp.Server;

namespace WBTL.Models
{
    public class PluginModel
    {
        #region Threads
        WebSocketServer wssv { get; set; }
        BackgroundWorker BackgroundWorker { get; set; }
        static PluginModel _instance;
        #endregion

        #region Threads
        #endregion

        #region Properties

        public static PluginModel GetInstance
        {
            get
            {
                return _instance ?? (_instance = new PluginModel());
            }
        }

        #endregion

        #region Ctors
        private PluginModel()
        {
            this.BackgroundWorker = new BackgroundWorker();
            this.BackgroundWorker.WorkerSupportsCancellation = true;
            this.BackgroundWorker.DoWork += this.DoWork;
        }

        private void InitializeWSS()
        {
            this.wssv = new WebSocketServer(WBTL.Config.WSSPort);
            this.wssv.AddWebSocketService<FileHistory>(@"/" + WBTL.Config.WSSName);
        }
        #endregion

        #region Methods
        public void GetLog(DateTime day)
        {
            FileList.GetInstance.List.Add(day);
            FileHistory.GetInstance.AskForLog();
        }

        public void Start()
        {
            WBTL.Initialize();
            if (!this.BackgroundWorker.IsBusy)
            {
                InitializeWSS();
                this.BackgroundWorker.RunWorkerAsync();
            }


        }

        public void Stop()
        {
            if (this.BackgroundWorker.IsBusy)
                this.BackgroundWorker.CancelAsync();
        }

        public void Restart()
        {
            this.Stop();
            this.Start();
        }

        public void TruncateLog()
        {
            throw new NotImplementedException();
        }

        private void DoWork(object s, DoWorkEventArgs e)
        {
            BackgroundWorker worker = s as BackgroundWorker;
            wssv.Start();

            while (true)
            {
                Thread.Sleep(400);
                if (worker.CancellationPending)
                {
                    wssv.Stop();
                    e.Cancel = true;
                    break;
                }

            }
        }
        #endregion
    }
}
