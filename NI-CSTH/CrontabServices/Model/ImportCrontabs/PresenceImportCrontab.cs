﻿using CrontabServices.Model.ImportCrontabs;
using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices.Model
{
    public class PresenceImportCrontab : CrontabImportBase<PresenceImportRecord>
    {
        public PresenceImportCrontab(Crontab crontab) : base(crontab)
        {
        }

        protected override void SaveToDb(PresenceImportRecord record)
        {
            using (var db = new DbModelContainer())
            {
                var account = db.AccountSet.Include("User")
                 .Where(x => x.FirstName == record.FirstName
                             && x.LastName == record.LastName
                             && x.User.Login == record.Login).SingleOrDefault();

                List<History> presenceHistories = new List<History>();

                for (DateTime i = record.From; i <= record.To; i = i.AddDays(1))
                {
                    presenceHistories.Add(new History()
                    {
                        AccountId = account.Id,
                        ExistAppHistories = record.ExistAppHistories,
                        ExistWebHistories = record.ExistWebHistories,
                        ExistAppFtp = record.ExistAppHistories,
                        ExistWebFtp = record.ExistWebHistories,
                        Day = i,
                    });
                }

                db.HistorySet.AddRange(presenceHistories);

                db.SaveChanges();

            }
        }

    }
}
