﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBModel;
using CrontabServices.Model;
using CrontabServices.Model.ImportCrontabs;

namespace CrontabServices.Model
{
    public class UserImportCrontab : CrontabImportBase<UsersImportRecord>
    {
        public UserImportCrontab(Crontab crontab) : base(crontab)
        {
        }

        protected override void SaveToDb(UsersImportRecord record)
        {
            using (var db = new DbModelContainer())
            {
                db.AccountSet.Add(new Account()
                {
                    FirstName = record.FirstName,
                    LastName = record.LastName,
                    User = new User()
                    {
                        IsActive = record.IsActive,
                        Password = record.Password,
                        Login = record.Login 
                    }
                });

                db.SaveChanges();
            } 
        }
    }
}
