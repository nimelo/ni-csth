﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.Classes.ATL
{
    class ATLEqualityComparer : IEqualityComparer<ATLEntry>
    {
        public bool Equals(ATLEntry x, ATLEntry y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(ATLEntry obj)
        {
            return 0;
        }
    }
}
