﻿using ATL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATL
{
    /*
    -h
    -loggingpath [path]
    -start
    -stop
    -restart              
        */
    public static class CommandLineParser
    {
        public static void Parse(string toParse)
        {
            switch (toParse)
            {
                case "-start":
                    PluginModel.GetInstance.Start();
                    break;
                case "-stop":
                    PluginModel.GetInstance.Stop();
                    break;
                case "-restart":
                    PluginModel.GetInstance.Restart();
                    break;
                case "-loggingPath":

                    break;
                case "-exit":
                    Environment.Exit(0);
                    break;
                default:

                    break;
            }
            Console.WriteLine(toParse);
        }
    }
}
