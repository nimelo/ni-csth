
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/29/2015 18:57:46
-- Generated from EDMX file: E:\VSOnlineRepos\NI-CSTH\DBModel\DbModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [NICSTH];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UsersAccounts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet] DROP CONSTRAINT [FK_UsersAccounts];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriesWebHistories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WebHistorySet] DROP CONSTRAINT [FK_HistoriesWebHistories];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriesAppHistories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AppHistorySet] DROP CONSTRAINT [FK_HistoriesAppHistories];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountFileProcessingHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FileProcessingHistorySet] DROP CONSTRAINT [FK_AccountFileProcessingHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistorySet] DROP CONSTRAINT [FK_AccountHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoryCategoryItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategoryItemSet] DROP CONSTRAINT [FK_CategoryCategoryItem];
GO
IF OBJECT_ID(N'[dbo].[FK_CrontabFileProcessingHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FileProcessingHistorySet] DROP CONSTRAINT [FK_CrontabFileProcessingHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_WebHistoryDomainsWebHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WebHistorySet] DROP CONSTRAINT [FK_WebHistoryDomainsWebHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_WebHistoryDomainsWebItemPeriod]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WebItemPeriodSet] DROP CONSTRAINT [FK_WebHistoryDomainsWebItemPeriod];
GO
IF OBJECT_ID(N'[dbo].[FK_WebHistoryWebItemPeriod]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WebHistorySet] DROP CONSTRAINT [FK_WebHistoryWebItemPeriod];
GO
IF OBJECT_ID(N'[dbo].[FK_AppHistoryAppItemPeriod]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AppHistorySet] DROP CONSTRAINT [FK_AppHistoryAppItemPeriod];
GO
IF OBJECT_ID(N'[dbo].[FK_AppHistoryDomainAppHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AppHistorySet] DROP CONSTRAINT [FK_AppHistoryDomainAppHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_AppHistoryDomainAppItemPeriod]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AppItemPeriodSet] DROP CONSTRAINT [FK_AppHistoryDomainAppItemPeriod];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoryWebHistoryDomain]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WebHistoryDomainSet] DROP CONSTRAINT [FK_HistoryWebHistoryDomain];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoryAppHistoryDomain]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AppHistoryDomainSet] DROP CONSTRAINT [FK_HistoryAppHistoryDomain];
GO
IF OBJECT_ID(N'[dbo].[FK_SpectrumCategoryCategoryItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategoryItemSet] DROP CONSTRAINT [FK_SpectrumCategoryCategoryItem];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO
IF OBJECT_ID(N'[dbo].[AccountSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccountSet];
GO
IF OBJECT_ID(N'[dbo].[HistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistorySet];
GO
IF OBJECT_ID(N'[dbo].[WebHistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WebHistorySet];
GO
IF OBJECT_ID(N'[dbo].[AppHistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AppHistorySet];
GO
IF OBJECT_ID(N'[dbo].[SystemParameterSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SystemParameterSet];
GO
IF OBJECT_ID(N'[dbo].[CrontabSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CrontabSet];
GO
IF OBJECT_ID(N'[dbo].[FileProcessingHistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FileProcessingHistorySet];
GO
IF OBJECT_ID(N'[dbo].[CategorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategorySet];
GO
IF OBJECT_ID(N'[dbo].[CategoryItemSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategoryItemSet];
GO
IF OBJECT_ID(N'[dbo].[WebItemPeriodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WebItemPeriodSet];
GO
IF OBJECT_ID(N'[dbo].[AppItemPeriodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AppItemPeriodSet];
GO
IF OBJECT_ID(N'[dbo].[WebHistoryDomainSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WebHistoryDomainSet];
GO
IF OBJECT_ID(N'[dbo].[AppHistoryDomainSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AppHistoryDomainSet];
GO
IF OBJECT_ID(N'[dbo].[SpectrumCategorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SpectrumCategorySet];
GO
IF OBJECT_ID(N'[dbo].[AppPathMapSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AppPathMapSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Account_Id] int  NOT NULL
);
GO

-- Creating table 'AccountSet'
CREATE TABLE [dbo].[AccountSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HistorySet'
CREATE TABLE [dbo].[HistorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Day] datetime  NOT NULL,
    [ExistWebHistories] bit  NOT NULL,
    [ExistAppHistories] bit  NOT NULL,
    [AccountId] int  NOT NULL,
    [ExistWebFtp] bit  NOT NULL,
    [ExistAppFtp] bit  NOT NULL
);
GO

-- Creating table 'WebHistorySet'
CREATE TABLE [dbo].[WebHistorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [HistoryId] int  NOT NULL,
    [Url] nvarchar(max)  NOT NULL,
    [Domain] nvarchar(max)  NOT NULL,
    [WebHistoryDomainsId] int  NOT NULL,
    [WebItemPeriod_Id] int  NOT NULL
);
GO

-- Creating table 'AppHistorySet'
CREATE TABLE [dbo].[AppHistorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [HistoryId] int  NOT NULL,
    [ProcessName] nvarchar(max)  NOT NULL,
    [Path] nvarchar(max)  NOT NULL,
    [AppHistoryDomainId] int  NOT NULL,
    [AppItemPeriod_Id] int  NOT NULL
);
GO

-- Creating table 'SystemParameterSet'
CREATE TABLE [dbo].[SystemParameterSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Key] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CrontabSet'
CREATE TABLE [dbo].[CrontabSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LastExecution] datetime  NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Interval] time  NOT NULL,
    [InputFolderName] nvarchar(max)  NOT NULL,
    [ErrorFolderName] nvarchar(max)  NOT NULL,
    [RootPath] nvarchar(max)  NOT NULL,
    [OutputFolderName] nvarchar(max)  NOT NULL,
    [IsActive] bit  NOT NULL
);
GO

-- Creating table 'FileProcessingHistorySet'
CREATE TABLE [dbo].[FileProcessingHistorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Status] tinyint  NOT NULL,
    [AccountId] int  NOT NULL,
    [CrontabId] int  NOT NULL,
    [ProcessingDate] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CategorySet'
CREATE TABLE [dbo].[CategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsRenderable] bit  NOT NULL
);
GO

-- Creating table 'CategoryItemSet'
CREATE TABLE [dbo].[CategoryItemSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UniqueIdentificator] nvarchar(max)  NOT NULL,
    [CategoryId] int  NULL,
    [Type] tinyint  NOT NULL,
    [SpectrumCategoryId] int  NULL
);
GO

-- Creating table 'WebItemPeriodSet'
CREATE TABLE [dbo].[WebItemPeriodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [From] datetime  NOT NULL,
    [To] datetime  NOT NULL,
    [TimeSpan] time  NOT NULL,
    [WebHistoryDomainsId] int  NOT NULL
);
GO

-- Creating table 'AppItemPeriodSet'
CREATE TABLE [dbo].[AppItemPeriodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [From] datetime  NOT NULL,
    [To] datetime  NOT NULL,
    [TimeSpan] time  NOT NULL,
    [AppHistoryDomainId] int  NOT NULL
);
GO

-- Creating table 'WebHistoryDomainSet'
CREATE TABLE [dbo].[WebHistoryDomainSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Domain] nvarchar(max)  NOT NULL,
    [HistoryId] int  NOT NULL,
    [SumTimeSpan] time  NOT NULL
);
GO

-- Creating table 'AppHistoryDomainSet'
CREATE TABLE [dbo].[AppHistoryDomainSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Path] nvarchar(max)  NOT NULL,
    [ProcessName] nvarchar(max)  NOT NULL,
    [HistoryId] int  NOT NULL,
    [SumTimeSpan] time  NOT NULL
);
GO

-- Creating table 'SpectrumCategorySet'
CREATE TABLE [dbo].[SpectrumCategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IsRenderable] bit  NOT NULL,
    [Type] tinyint  NOT NULL
);
GO

-- Creating table 'AppPathMapSet'
CREATE TABLE [dbo].[AppPathMapSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Path] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AccountSet'
ALTER TABLE [dbo].[AccountSet]
ADD CONSTRAINT [PK_AccountSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [PK_HistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WebHistorySet'
ALTER TABLE [dbo].[WebHistorySet]
ADD CONSTRAINT [PK_WebHistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AppHistorySet'
ALTER TABLE [dbo].[AppHistorySet]
ADD CONSTRAINT [PK_AppHistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SystemParameterSet'
ALTER TABLE [dbo].[SystemParameterSet]
ADD CONSTRAINT [PK_SystemParameterSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CrontabSet'
ALTER TABLE [dbo].[CrontabSet]
ADD CONSTRAINT [PK_CrontabSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FileProcessingHistorySet'
ALTER TABLE [dbo].[FileProcessingHistorySet]
ADD CONSTRAINT [PK_FileProcessingHistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategorySet'
ALTER TABLE [dbo].[CategorySet]
ADD CONSTRAINT [PK_CategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategoryItemSet'
ALTER TABLE [dbo].[CategoryItemSet]
ADD CONSTRAINT [PK_CategoryItemSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WebItemPeriodSet'
ALTER TABLE [dbo].[WebItemPeriodSet]
ADD CONSTRAINT [PK_WebItemPeriodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AppItemPeriodSet'
ALTER TABLE [dbo].[AppItemPeriodSet]
ADD CONSTRAINT [PK_AppItemPeriodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WebHistoryDomainSet'
ALTER TABLE [dbo].[WebHistoryDomainSet]
ADD CONSTRAINT [PK_WebHistoryDomainSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AppHistoryDomainSet'
ALTER TABLE [dbo].[AppHistoryDomainSet]
ADD CONSTRAINT [PK_AppHistoryDomainSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SpectrumCategorySet'
ALTER TABLE [dbo].[SpectrumCategorySet]
ADD CONSTRAINT [PK_SpectrumCategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AppPathMapSet'
ALTER TABLE [dbo].[AppPathMapSet]
ADD CONSTRAINT [PK_AppPathMapSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Account_Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [FK_UsersAccounts]
    FOREIGN KEY ([Account_Id])
    REFERENCES [dbo].[AccountSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersAccounts'
CREATE INDEX [IX_FK_UsersAccounts]
ON [dbo].[UserSet]
    ([Account_Id]);
GO

-- Creating foreign key on [HistoryId] in table 'WebHistorySet'
ALTER TABLE [dbo].[WebHistorySet]
ADD CONSTRAINT [FK_HistoriesWebHistories]
    FOREIGN KEY ([HistoryId])
    REFERENCES [dbo].[HistorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoriesWebHistories'
CREATE INDEX [IX_FK_HistoriesWebHistories]
ON [dbo].[WebHistorySet]
    ([HistoryId]);
GO

-- Creating foreign key on [HistoryId] in table 'AppHistorySet'
ALTER TABLE [dbo].[AppHistorySet]
ADD CONSTRAINT [FK_HistoriesAppHistories]
    FOREIGN KEY ([HistoryId])
    REFERENCES [dbo].[HistorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoriesAppHistories'
CREATE INDEX [IX_FK_HistoriesAppHistories]
ON [dbo].[AppHistorySet]
    ([HistoryId]);
GO

-- Creating foreign key on [AccountId] in table 'FileProcessingHistorySet'
ALTER TABLE [dbo].[FileProcessingHistorySet]
ADD CONSTRAINT [FK_AccountFileProcessingHistory]
    FOREIGN KEY ([AccountId])
    REFERENCES [dbo].[AccountSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountFileProcessingHistory'
CREATE INDEX [IX_FK_AccountFileProcessingHistory]
ON [dbo].[FileProcessingHistorySet]
    ([AccountId]);
GO

-- Creating foreign key on [AccountId] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [FK_AccountHistory]
    FOREIGN KEY ([AccountId])
    REFERENCES [dbo].[AccountSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountHistory'
CREATE INDEX [IX_FK_AccountHistory]
ON [dbo].[HistorySet]
    ([AccountId]);
GO

-- Creating foreign key on [CategoryId] in table 'CategoryItemSet'
ALTER TABLE [dbo].[CategoryItemSet]
ADD CONSTRAINT [FK_CategoryCategoryItem]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoryCategoryItem'
CREATE INDEX [IX_FK_CategoryCategoryItem]
ON [dbo].[CategoryItemSet]
    ([CategoryId]);
GO

-- Creating foreign key on [CrontabId] in table 'FileProcessingHistorySet'
ALTER TABLE [dbo].[FileProcessingHistorySet]
ADD CONSTRAINT [FK_CrontabFileProcessingHistory]
    FOREIGN KEY ([CrontabId])
    REFERENCES [dbo].[CrontabSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CrontabFileProcessingHistory'
CREATE INDEX [IX_FK_CrontabFileProcessingHistory]
ON [dbo].[FileProcessingHistorySet]
    ([CrontabId]);
GO

-- Creating foreign key on [WebHistoryDomainsId] in table 'WebHistorySet'
ALTER TABLE [dbo].[WebHistorySet]
ADD CONSTRAINT [FK_WebHistoryDomainsWebHistory]
    FOREIGN KEY ([WebHistoryDomainsId])
    REFERENCES [dbo].[WebHistoryDomainSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WebHistoryDomainsWebHistory'
CREATE INDEX [IX_FK_WebHistoryDomainsWebHistory]
ON [dbo].[WebHistorySet]
    ([WebHistoryDomainsId]);
GO

-- Creating foreign key on [WebHistoryDomainsId] in table 'WebItemPeriodSet'
ALTER TABLE [dbo].[WebItemPeriodSet]
ADD CONSTRAINT [FK_WebHistoryDomainsWebItemPeriod]
    FOREIGN KEY ([WebHistoryDomainsId])
    REFERENCES [dbo].[WebHistoryDomainSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WebHistoryDomainsWebItemPeriod'
CREATE INDEX [IX_FK_WebHistoryDomainsWebItemPeriod]
ON [dbo].[WebItemPeriodSet]
    ([WebHistoryDomainsId]);
GO

-- Creating foreign key on [WebItemPeriod_Id] in table 'WebHistorySet'
ALTER TABLE [dbo].[WebHistorySet]
ADD CONSTRAINT [FK_WebHistoryWebItemPeriod]
    FOREIGN KEY ([WebItemPeriod_Id])
    REFERENCES [dbo].[WebItemPeriodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WebHistoryWebItemPeriod'
CREATE INDEX [IX_FK_WebHistoryWebItemPeriod]
ON [dbo].[WebHistorySet]
    ([WebItemPeriod_Id]);
GO

-- Creating foreign key on [AppItemPeriod_Id] in table 'AppHistorySet'
ALTER TABLE [dbo].[AppHistorySet]
ADD CONSTRAINT [FK_AppHistoryAppItemPeriod]
    FOREIGN KEY ([AppItemPeriod_Id])
    REFERENCES [dbo].[AppItemPeriodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AppHistoryAppItemPeriod'
CREATE INDEX [IX_FK_AppHistoryAppItemPeriod]
ON [dbo].[AppHistorySet]
    ([AppItemPeriod_Id]);
GO

-- Creating foreign key on [AppHistoryDomainId] in table 'AppHistorySet'
ALTER TABLE [dbo].[AppHistorySet]
ADD CONSTRAINT [FK_AppHistoryDomainAppHistory]
    FOREIGN KEY ([AppHistoryDomainId])
    REFERENCES [dbo].[AppHistoryDomainSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AppHistoryDomainAppHistory'
CREATE INDEX [IX_FK_AppHistoryDomainAppHistory]
ON [dbo].[AppHistorySet]
    ([AppHistoryDomainId]);
GO

-- Creating foreign key on [AppHistoryDomainId] in table 'AppItemPeriodSet'
ALTER TABLE [dbo].[AppItemPeriodSet]
ADD CONSTRAINT [FK_AppHistoryDomainAppItemPeriod]
    FOREIGN KEY ([AppHistoryDomainId])
    REFERENCES [dbo].[AppHistoryDomainSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AppHistoryDomainAppItemPeriod'
CREATE INDEX [IX_FK_AppHistoryDomainAppItemPeriod]
ON [dbo].[AppItemPeriodSet]
    ([AppHistoryDomainId]);
GO

-- Creating foreign key on [HistoryId] in table 'WebHistoryDomainSet'
ALTER TABLE [dbo].[WebHistoryDomainSet]
ADD CONSTRAINT [FK_HistoryWebHistoryDomain]
    FOREIGN KEY ([HistoryId])
    REFERENCES [dbo].[HistorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryWebHistoryDomain'
CREATE INDEX [IX_FK_HistoryWebHistoryDomain]
ON [dbo].[WebHistoryDomainSet]
    ([HistoryId]);
GO

-- Creating foreign key on [HistoryId] in table 'AppHistoryDomainSet'
ALTER TABLE [dbo].[AppHistoryDomainSet]
ADD CONSTRAINT [FK_HistoryAppHistoryDomain]
    FOREIGN KEY ([HistoryId])
    REFERENCES [dbo].[HistorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryAppHistoryDomain'
CREATE INDEX [IX_FK_HistoryAppHistoryDomain]
ON [dbo].[AppHistoryDomainSet]
    ([HistoryId]);
GO

-- Creating foreign key on [SpectrumCategoryId] in table 'CategoryItemSet'
ALTER TABLE [dbo].[CategoryItemSet]
ADD CONSTRAINT [FK_SpectrumCategoryCategoryItem]
    FOREIGN KEY ([SpectrumCategoryId])
    REFERENCES [dbo].[SpectrumCategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SpectrumCategoryCategoryItem'
CREATE INDEX [IX_FK_SpectrumCategoryCategoryItem]
ON [dbo].[CategoryItemSet]
    ([SpectrumCategoryId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------