﻿using System;
using System.Runtime.Serialization;

namespace CrontabServices.Model
{
    [Serializable]
    public class InvalidRecordProperitesCountException : Exception
    {
        public InvalidRecordProperitesCountException()
        {
        }

        public InvalidRecordProperitesCountException(string message) : base(message)
        {
        }

        public InvalidRecordProperitesCountException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidRecordProperitesCountException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string ToString()
        {
            return "InvalidRecordProperitesCount";
        }
    }
}