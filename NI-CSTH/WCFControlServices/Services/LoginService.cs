﻿using DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFControlServices.Interfaces;

namespace WCFControlServices.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in both code and config file together.
    public class LoginService : ILoginService
    {
        public bool Log(string login, string encryptedPassword)
        {
            bool isPresent = false;

            using (var db = new DbModelContainer())
            {
                try
                {
                    isPresent = db.UserSet.Where(x => x.Login == login && x.Password == encryptedPassword).Count() == 1;
                }
                catch(Exception e)
                {

                }
            }

            return isPresent;
        }
    }
}
