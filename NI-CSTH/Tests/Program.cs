﻿using System.Data.Entity;
using DBModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.IO.Pipes;
using System.Threading;
using System.Net;
using Titanium.Web.Proxy;
using Titanium.Web.Proxy.EventArguments;
using System.Text.RegularExpressions;
using NDde.Client;
using System.Runtime.InteropServices;
using System.Diagnostics;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using System.Windows.Automation;

namespace Tests
{
    class Program
    {

        public static void Main(string[] args)
        {
            while (true)
            {

                Console.Clear();
                try
                {
                    foreach (Process process in Process.GetProcessesByName("firefox"))
                    {
                        string url = GetFirefoxUrl(process);
                        if (url == null)
                            continue;

                        // Console.WriteLine("FF Url for '" + process.MainWindowTitle + "' is " + url);
                        Console.WriteLine($"FF {url}");
                    }

                    foreach (Process process in Process.GetProcessesByName("iexplore"))
                    {
                        try
                        {
                            string url = GetInternetExplorerUrl(process);
                            if (url == null)
                                continue;

                            // Console.WriteLine("IE Url for '" + process.MainWindowTitle + "' is " + url);
                            Console.WriteLine($"IE {url}");
                        }
                        catch (Exception)
                        {

                        }

                    }

                    foreach (Process process in Process.GetProcessesByName("opera"))
                    {
                        try
                        {
                            string url = GetOperaUrl(process);
                            if (url == null)
                                continue;

                            // Console.WriteLine("IE Url for '" + process.MainWindowTitle + "' is " + url);
                            Console.WriteLine($"OPERA {url}");
                        }
                        catch (Exception)
                        {

                        }

                    }

                    foreach (Process process in Process.GetProcessesByName("chrome"))
                    {
                        try
                        {
                            string url = GetChromeUrl(process);
                            if (url == null)
                                continue;

                            // Console.WriteLine("CH Url for '" + process.MainWindowTitle + "' is " + url);
                            Console.WriteLine($"Chrome {url}");
                        }
                        catch (Exception)
                        {

                        }



                    }
                }
                catch (Exception)
                {

                    // throw;
                }
                Thread.Sleep(1000);
            }
        }


        public static string GetOperaUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            //element.FindAll(TreeScope.)
            AutomationElement edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }

        public static string GetChromeUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElement edit = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }

        public static string GetInternetExplorerUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElement rebar = element.FindFirst(TreeScope.Children, new PropertyCondition(AutomationElement.ClassNameProperty, "ReBarWindow32"));
            if (rebar == null)
                return null;

            AutomationElement edit = rebar.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));

            return ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }

        public static string GetFirefoxUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElement doc = element.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            if (doc == null)
                return null;

            return ((ValuePattern)doc.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
        }
     
    }
}
