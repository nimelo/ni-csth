﻿using CrontabServices.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices.Runer
{
    public static class CrontabRuner
    {
        public static void Run(CrontabBase crontab)
        {
            Task.Run(() =>
            {
                crontab.DoWork();
            });
        }
    }
}
