﻿using DBModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CrontabServices.Model
{
    public abstract class CrontabLogBase<T> : CrontabBase where T : class
    {
        #region Properties
        protected List<History> UserHistories { get; set; }
        #endregion

        #region Public Methods
        public override void DoWork()
        {
            List<string> loginList = this.GetLoginList();

            loginList.ForEach(x => this.Preprocess(x));
        }
        #endregion

        #region Virtual Methods
        protected virtual void Preprocess(string userName)
        {
            Console.WriteLine("Preprocessing log for: " + userName);

            using (var db = new DbModelContainer())
            {
                UserHistories = db.HistorySet.Where(x => x.Account.User.Login == userName).ToList();
                // db.UserSet.Include(y => y.Account).Include(z => z.Account.Histories).Where(u => u.Login == userName).Single().Account.Histories.ToList();
            }

            if (UserHistories.Count() == 0) return;

            var inPath = Path.Combine(this.Crontab.RootPath, userName, this.Crontab.InputFolderName);
            var errorPath = Path.Combine(this.Crontab.RootPath, userName, this.Crontab.ErrorFolderName);
            var outPath = Path.Combine(this.Crontab.RootPath, userName, this.Crontab.OutputFolderName);

            this.CreateDirectories(inPath, errorPath, outPath);

            var files = Directory.GetFiles(inPath);
            var approvedFiles = SeparateApprovedFiles(files);

            foreach (var file in approvedFiles)
            {
                var log = File.ReadAllText(file);
                var history = this.UserHistories.Where(h => h.Day.ToShortDateString() == Path.GetFileName(file).Replace(".log", string.Empty)).Single();
                try
                {
                    this.ProcessLog(log, history);
                    File.Move(file, Path.Combine(outPath, Path.GetFileName(file)));
                    this.SaveLogStatusToDB(Path.GetFileName(file), LogStatusEnum.OK, history.AccountId);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error in file: " + userName);
                    File.Move(file, Path.Combine(errorPath, Path.GetFileName(file)));
                    this.SaveLogStatusToDB(Path.GetFileName(file), LogStatusEnum.Error, history.AccountId);
                }
            }
        }

        protected abstract IEnumerable<string> SeparateApprovedFiles(string[] files);

        private void CreateDirectories(string inPath, string errorPath, string outPath)
        {
            if (!Directory.Exists(inPath))
            {
                Directory.CreateDirectory(inPath);
            }

            if (!Directory.Exists(errorPath))
            {
                Directory.CreateDirectory(errorPath);
            }

            if (!Directory.Exists(outPath))
            {
                Directory.CreateDirectory(outPath);
            }
        }

        protected virtual void ProcessLog(string log, History history)
        {
            List<T> newWebHistory = new List<T>();

            newWebHistory = this.ExtractLog(log, history);

            this.SaveToDB(newWebHistory, history.Id);
        }

        #endregion

        #region Abstract Methods
        protected abstract List<T> ExtractLog(string log, History history);

        protected abstract void SaveToDB(List<T> newWebHistory, int historyId);

        protected virtual void SaveLogStatusToDB(string fileName, LogStatusEnum status, int accountId)
        {
            using (var db = new DbModelContainer())
            {
                FileProcessingHistory entry = new FileProcessingHistory()
                {
                    AccountId = accountId,
                    Name = fileName,
                    Status = (byte)status,
                    CrontabId = this.Crontab.Id,
                    ProcessingDate = DateTime.Now.ToShortDateString()
                };

                db.FileProcessingHistorySet.Add(entry);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {

                    throw;
                }
            }
        }
        #endregion

        #region Common Methods
        protected List<string> GetLoginList()
        {
            List<string> loginList = new List<string>();
            using (var db = new DbModelContainer())
            {
                loginList = db.UserSet.Where(y => y.IsActive == true).Select(x => x.Login).ToList();
            }

            return loginList;
        }
        #endregion

        #region Ctors
        public CrontabLogBase(Crontab crontab) : base(crontab)
        {
            this.Crontab = crontab;
        }
        #endregion
    }
}
