﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WBTL.Queue
{
    public class FileList
    {
        private static FileList _instance;

        public static FileList GetInstance
        {
            get
            {
                return _instance ?? (_instance = new FileList());
            }
        }

        private FileList()
        {
            this.List = new List<DateTime>();
        }

        public List<DateTime> List { get; private set; }
    }
}
