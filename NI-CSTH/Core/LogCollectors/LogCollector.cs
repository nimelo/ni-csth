﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagmentApplication.LogCollectors
{
    public class LogCollector
    {
        #region Fields
        public static LogCollector _instance;
        #endregion

        #region Properties
        public static LogCollector GetInstance { get { return _instance ?? (_instance = new LogCollector()); } }
        #endregion

        #region Ctors
        private LogCollector()
        {

        }
        #endregion

        #region Public Methods
        public string Collect(string fileName)
        {
            try
            {
                return System.IO.File.ReadAllText(fileName);
            }
            catch (Exception e)
            {
                return null;
            }

        }
        #endregion
    }
}
