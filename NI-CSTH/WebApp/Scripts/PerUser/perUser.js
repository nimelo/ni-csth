﻿$(function () {

    $('#find').on('click', function (event) {
        event.preventDefault();
        var fName = $('#inFirstName').val().trim();
        var lName = $('#inLastName').val().trim();

        $('#userInformations').hide();

        getRelatedUsers(fName, lName, renderUsersTable);       
    })

    function getLastCategories() {
        $.get("/Categories/GetLastCategoryApp", { login: window.userLogin }, function (data) {
            console.log(data)
            ChartsRendering.renderPie('#app', data);
        }).fail(function () {
            alert('fail')
        })

        $.get("/Categories/GetLastCategoryWeb", { login: window.userLogin }, function (data) {
            console.log(data)
            ChartsRendering.renderPie('#web', data);
        }).fail(function () {
            alert('fail')
        })
    }

    function getRelatedUsers(fName, lName, callback) {
        $.get("/PerUser/ExistUser", { firstName: fName, lastName: lName }, function (data) {
            console.log(data);
            if (data.length != 0) {
                $('#findControl').removeClass('alert alert-danger');
                $('#findControl').addClass('alert alert-success');
                callback(data);
            } else {
                $('#findControl').removeClass('alert alert-success');
                $('#findControl').addClass('alert alert-danger');
            }
        }).fail(function () {
            alert('fail')
        })
    }

    function renderUsersTable(data) {
        html = "";

        $.each(data, function (index, el) {
            html += '<tr class="user" login="' + el.User.Login + '">';
            html += "<td>" + el.FirstName + "</td>"
            html += "<td>" + el.LastName + "</td>"
            html += "<td>" + el.User.Login + "</td>"
            html += "</tr>"
        });

        $('#usersTable tbody').html(html);

        userTableClick();

        $('#usersTable').show();
    }

    function userTableClick() {
        $('.user').on('click', function (event) {
            event.preventDefault();
            var login = $(this).attr('login');
            window.userLogin = login;

            $('#usersTable').hide();

            $('#userInformations').show();
            renderInformations();

        })
    }

    function renderInformations() {
        $('#chartsSpectrum').html("");
        $('#chartsCategory').html("");
        getRenderableSpectrums();
        getRenderableCategories();
        getLastCategories();
    }

    /*Spectrums*/
    function getRenderableSpectrums() {
        $.get("/Spectrums/GetRenderableSpectrums", {}, function (data) {

            $.each(data, function (index, el) {
                addSpectrum(el.Name, el.Id);
                getSpectrumData(el.Id);
            })

        }).fail(function () {
            alert('fail')
        })
    }

    function getSpectrumData(id) {
        $.get("/Spectrums/GetChartDataForSpectrum", { spectrumId: id, from: "", to: "", login: window.userLogin, forAll: false }, function (data) {
            console.log(data)
            ChartsRendering.renderColumn('#spectrum' + id, data.data, data.categories);
        }).fail(function () {
            alert('fail')
        })
    }

    function addSpectrum(name, id) {
        html = ""
        html += '<div class="' + 'center' + 'half">'
        html += '<h2 class="text-center">' + name + '</h2>'
        html += ' <div id="spectrum' + id + '" class="basic-chart"></div></div>'
        html += '<hr />'
        $('#chartsSpectrum').append(html);
    }


    /*Categories*/
    function getRenderableCategories() {
        $.get("/Categories/GetRenderableCategories", {}, function (data) {
            $.each(data, function (index, el) {
                addCategory(el.Name, el.Id, index, data.length);
                getCategoryData(el.Id);
            })

        }).fail(function () {
            alert('fail')
        })
    }

    function getCategoryData(id) {
        $.get("/Categories/GetChartDataForCategory", { categoryId: id, from: "", to: "", login: window.userLogin, forAll: false }, function (data) {
            ChartsRendering.renderPie('#category' + id, data);
        }).fail(function () {
            alert('fail')
        })
    }

    function addCategory(name, id, index, length) {
        html = ""
        if (index + 1 == length && length % 2 != 0) {
            html += '<div class="center half">'
        }
        else {
            html += '<div class="left half">'
        }

        html += '<h2 class="text-center">' + name + '</h2>'
        html += ' <div id="category' + id + '" class="basic-chart"></div></div>'

        $('#chartsCategory').append(html);
    }

    $('#userInformations').hide();
    $('#usersTable').hide();
})