﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ATL.Models
{
    public class AppInformation
    {
        #region Properties
        public string WindowText { get; set; }

        public string ProcessName { get; set; }

        public string FileDescription { get; set; }

        public string ModuleName { get; set; }

        public string FileVersion { get; set; }

        public string CompanyName { get; set; }

        public string FileName { get; set; }

        #endregion

        #region Ctors
        public AppInformation()
        {

        }
        #endregion

        #region Static Methods
        public static AppInformation ExtractFromWindow(IntPtr hWnd)
        {
            uint processId;
            GetWindowThreadProcessId(hWnd, out processId);

            if (processId == 0 || processId == 4 || processId == 8)
                throw new Exception();

            Process p = Process.GetProcessById((int)processId);

            return new AppInformation()
            {
                WindowText = GetWindowText(hWnd),
                ProcessName = p.ProcessName,
                FileDescription = p.MainModule.FileVersionInfo.FileDescription,
                ModuleName = p.MainModule.ModuleName,
                FileVersion = p.MainModule.FileVersionInfo.FileVersion,
                CompanyName = p.MainModule.FileVersionInfo.CompanyName,
                FileName = p.Modules[0].FileName
            };

        }

        #endregion

        #region DLL imports
        [DllImport("user32.dll")]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

        [DllImport("user32.dll")]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        #endregion

        #region Private Methods
        private static string GetWindowText(IntPtr hWnd)
        {
            int lengthOfText = GetWindowTextLength(hWnd) + 1;

            StringBuilder sb = new StringBuilder(lengthOfText);
            GetWindowText(hWnd, sb, lengthOfText);

            return sb.ToString();
        }
        #endregion

        #region Public Methods
        public string ToHumanString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var prop in this.GetType().GetProperties())
            {
                sb.AppendLine(string.Format("{0} - {1}", prop.Name, prop.GetValue(this, null)));
            }

            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.ProcessName).Append("|").Append(this.FileName);
            //foreach (var prop in this.GetType().GetProperties())
            //{
            //    sb.Append(string.Format("{0}", prop.GetValue(this, null))).Append("|");
            //}

            return sb.ToString();
        }
        #endregion
        #region Consts

        #endregion

    }
}
