﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ATL.Models;
using Moq;
using CrontabServices.Model;

namespace UnitTests
{
    [TestClass]
    public class CrontabImportTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var header = "abcd|0";
            var fileName = "abcd";
            var amount = worker.Object.ProcessHeader(header, fileName);

            Assert.AreEqual(0, amount);
        }

        [TestMethod]
        public void TestMethod6()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var header = "abcd|45";
            var fileName = "abc";
            int amount = 0;
            try
            {
                amount = worker.Object.ProcessHeader(header, fileName);
            }
            catch (InvalidHeaderExcepction e)
            {

                Assert.AreEqual(typeof(InvalidHeaderExcepction), e.GetType());
            }
           

            Assert.AreEqual(0, amount);
        }

        [TestMethod]
        public void TestMethod7()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var header = "abcd|4k5";
            var fileName = "abc";
            int amount = 0;
            try
            {
                amount = worker.Object.ProcessHeader(header, fileName);
            }
            catch (InvalidHeaderExcepction e)
            {

                Assert.AreEqual(typeof(InvalidHeaderExcepction), e.GetType());
            }

            Assert.AreEqual(0, amount);
        }

        [TestMethod]
        public void TestMethod8()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var header = "";
            var fileName = "abc";
            int amount = 0;
            try
            {
                amount = worker.Object.ProcessHeader(header, fileName);
            }
            catch (InvalidHeaderExcepction e)
            {

                Assert.AreEqual(typeof(InvalidHeaderExcepction), e.GetType());
            }

            Assert.AreEqual(0, amount);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "abcd|asdfadsf|asdfasdfa|17/11/1993|18/11/1993|true|true";

            worker.CallBase = true;
            var amount = worker.Object.ProcessRecord(record);

            Assert.IsNotNull(amount);
        }

        [TestMethod]
        public void TestMethod3()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "abcd|asdfadsf|asdfasdfa|17/11/1993|18/11/1993|true|";
            AbstractRecord amount = null;
            worker.CallBase = true;
            try
            {
                amount = worker.Object.ProcessRecord(record);
            }
            catch (InvalidRecordFieldExcepction e)
            {
                Assert.AreEqual(typeof(InvalidRecordFieldExcepction), e.GetType());
            }

            Assert.IsNull(amount);
        }

        [TestMethod]
        public void TestMethod4()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "abcd|asdfadsf|asdfasdfa|17/11/1993|18/11/1993|true|true|true|3|true|3|true|";
            AbstractRecord amount = null;
            worker.CallBase = true;
            try
            {
                amount = worker.Object.ProcessRecord(record);
            }
            catch (InvalidRecordProperitesCountException e)
            {
                Assert.AreEqual(typeof(InvalidRecordProperitesCountException), e.GetType());
            }

            Assert.IsNotNull(amount);
        }

        [TestMethod]
        public void TestMethod5()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "";
            AbstractRecord amount = null;
            worker.CallBase = true;
            try
            {
                amount = worker.Object.ProcessRecord(record);
            }
            catch (InvalidRecordProperitesCountException e)
            {
                Assert.AreEqual(typeof(InvalidRecordProperitesCountException), e.GetType());
            }

            Assert.IsNull(amount);
        }

        [TestMethod]
        public void TestMethod9()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "||";
            AbstractRecord amount = null;
            worker.CallBase = true;
            try
            {
                amount = worker.Object.ProcessRecord(record);
            }
            catch (InvalidRecordProperitesCountException e)
            {
                Assert.AreEqual(typeof(InvalidRecordProperitesCountException), e.GetType());
            }

            Assert.IsNull(amount);
        }

        [TestMethod]
        public void TestMethod10()
        {
            Mock<PresenceImportCrontab> worker = new Mock<PresenceImportCrontab>(null);
            var record = "abcd|asdfadsf|asdfasdfa|17/11/1993|18/11/1993|1|1";
            AbstractRecord amount = null;
            worker.CallBase = true;
            try
            {
                amount = worker.Object.ProcessRecord(record);
            }
            catch (InvalidRecordFieldExcepction e)
            {
                Assert.AreEqual(typeof(InvalidRecordFieldExcepction), e.GetType());
            }

            Assert.IsNull(amount);
        }

    }
}
