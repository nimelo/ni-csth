﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFControlServices.Interfaces;

namespace WCFControlServices.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ConfigurationService" in both code and config file together.
    public class ConfigurationService : IConfigurationService
    {
        public Dictionary<string, string> GetConfiguration()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            using (var db = new DBModel.DbModelContainer())
            {
                db.SystemParameterSet.ToList().ForEach(x => parameters.Add(x.Key, x.Value));
            }

            return parameters;
        }

        public bool IsConfigurationActual(string hash)
        {
            bool isConfigurationActual = false;

            using (var db = new DBModel.DbModelContainer())
            {
                isConfigurationActual = db.SystemParameterSet.Where(x => x.Key == "ConfigurationVersion").First().Value == hash;
            }

            return isConfigurationActual;
        }
    }
}
