﻿$(function () {


    function rem(el) {
        el.remove();
    }

    function renderCategoryAdd() {
        var html = "";
        html += '<tr class="edit alert alert-info">'

        html += '<td>' + '<input id="inNameCategory" type="text" class="form-control" style="display: inline-block;"/>' + '</td>'
        html += '<td>' + '<select id="inIsRenderableCategory" class="form-control"><option value="1">Yes</option><option value="0">No</option></select>' + '</td>'

        html += '<td>' + '<button class="addCategory btn glyphicon glyphicon-ok-circle"></button>' + '</td>'

        html += '</tr>'
        return html;
    }

    function renderCategoryEdit() {
        var html = "";
        html += '<tr class="edit alert alert-info">'

        html += '<td>' + '<input id="inNameCategoryEdit" type="text" class="form-control" style="display: inline-block;"/>' + '</td>'
        html += '<td>' + '<select id="inIsRenderableCategoryEdit" class="form-control" style="min-width:90px"><option value="1">Yes</option><option value="0">No</option></select>' + '</td>'

        html += '<td>' + '<button class="editCategory btn glyphicon glyphicon-ok-circle"></button>' + '</td>'

        html += '</tr>'
        return html;
    }

    /*Categories*/

    function getCategories(callback) {
        $.get("/CategoriesEdit/GetCategories", {}, function (data) {
            console.log(data)
            callback(data)
        }).fail(function () {
            alert('fail')
        })
    }

    function renderCategories(list) {
        html = "";
        $.each(list, function (index, el) {
            html += '<tr ids="' + el.Id + '" class="category">'
            if (el.Type == 0) {
                el.Type = "App"
            }
            else {
                el.Type = "Web"
            }

            html += '<td>' + el.Name + '</td>'
            html += '<td>' + (el.IsRenderable ? 'Yes' : 'No') + '</td>'
            html += '</tr>'
        })
        html += renderCategoryAdd();
        $('#categoriesContainer tbody').html(html);
        categoryClick();
        categoryAddClick();

    }

    function rowCategoryToJson(row) {
        var json = Object();

        json.name = row.querySelector('td:nth-child(1)').innerHTML
        json.isRenderable = row.querySelector('td:nth-child(2)').innerHTML

        return json;
    }

    function deleteCategory(id, callback) {
        $.post("CategoriesEdit/DeleteCategory", { categoryId: id }, function (data) {
            callback();
        }).fail(function () {
            //alert('fail');
        })
    }

    /*Category Items*/
    function renderCategoryItems(list, categoryId) {

        html = "";
        $.each(list, function (index, el) {
            html += '<tr cId="' + el.CategoryId.toString() + '"' + 'myId="' + el.Id.toString() + '">'
            html += '<td>' + el.UniqueIdentificator + '</td>'
            html += '<td>' + (el.Type != 0 ? 'Web' : 'App') + '</td>'
            html += '<td><button class="removeCategoryItem glyphicon glyphicon-remove-circle btn"></button></td>'
            html += '</tr>'
        })
        html += '<tr ' + 'cid="' + categoryId + '"> <td> <input id="inUniqueCategoryItem" type="text" class="form-control" style="display: inline-block;"/></td><td><select id="inTypeCategoryItem" class="form-control"><option value="1">Web</option><option value="0">App</option></select>' + '</td> <td> <button class=" addCategoryItem     glyphicon glyphicon-ok-circle btn"></button>  </td>  </tr>'
        return html;
    }

    function GetCategoryItemsBy(id, callback) {
        $.get("/CategoriesEdit/GetCategoryItemsByCategoryId", { categoryId: id }, function (data) {
            console.log(data)
            callback(data)
        }).fail(function () {
            alert('fail')
        })
    }

    function deleteCategoryItem(categoryId, id, callback, callbackFail) {
        $.post("CategoriesEdit/DeleteCategoryItem", { categoryId: categoryId, id: id }, function (data) {
            callback();
        }).fail(function () {
            //alert('fail');
            callbackFail();
        })
    }

    function addCategoryItem(categoryId, identifier, type, callback, callbackFail) {
        $.post("CategoriesEdit/AddCategoryItem", { categoryId: categoryId, identifier: identifier, itemType: type }, function (data) {
            callback();
        }).fail(function () {
            callbackFail();
            //alert('fail');
        })

    }

    /*Popup js*/
    function categoryClick() {

        //open popup
        $('.category').on('dblclick', function (event) {
            event.preventDefault();

            var id = parseInt($(this).attr('ids'));
            var category = $(this);
            GetCategoryItemsBy(id, function (data) {
                $('#categoryItemsContainer tbody').html(renderCategoryItems(data, id));
                categoryItemClick();
            });
            $('#header').html($(this)[0].querySelector('td:nth-child(1)').innerHTML)
            $('#categoryItemsPopup').attr({ categoryId: id });
            $('#categoryItemsPopup').addClass('is-visible');
        });

        //close popup
        $('#categoryItemsPopup').on('click', function (event) {
            if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.hideCategory')) {
                event.preventDefault();
                $(this).removeClass('is-visible');
            }
        });

        //close popup when clicking the esc keyboard button
        $(document).keyup(function (event) {
            if (event.which == '27') {
                $('#categoryItemsPopup').removeClass('is-visible');
                $('#categoryPopup').removeClass('is-visible');
            }
        });

        $('.deleteCategoryItemsPopup').on('click', function () {

            $('#categoryItemsPopup').removeClass('is-visible');
            deleteCategory($('#categoryItemsPopup').attr('categoryId'),
             function () {
                 getCategories(renderCategories);
                 $('#categoryItemsPopup').removeClass('is-visible');
             },
             function () {

             });
        })

        $('.deleteCategory').on('click', function () {

            $('#categoryPopup').removeClass('is-visible');
            deleteCategory($('#categoryPopup').attr('categoryId'),
             function () {
                 getCategories(renderCategories);
                 $('#categoryPopup').removeClass('is-visible');
             },
             function () {

             });
        })

        $('.category').on('contextmenu', function (event) {
            event.preventDefault();
            var id = parseInt($(this).attr('ids'));
            var category = $(this);

            $('#inNameCategoryEdit').val($(this)[0].querySelector('td:nth-child(1)').innerHTML)

            $('#categoryPopupContainer tbody').html(renderCategoryEdit(rowCategoryToJson($(this)[0], id)));
            categoryEditClick();
            $('#inNameCategoryEdit').val($(this)[0].querySelector('td:nth-child(1)').innerHTML)
            $('#headerEdit').html('Edit ' + $(this)[0].querySelector('td:nth-child(1)').innerHTML)
            $('#categoryPopup').attr({
                categoryId: id
            });
            $('#categoryPopup').addClass('is-visible');
        });

        //close popup
        $('#categoryPopup').on('click', function (event) {
            if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.hideCategory')) {
                event.preventDefault();
                $(this).removeClass('is-visible');
            }
        })

    }

    function categoryItemClick() {
        $('.removeCategoryItem').on('click', function (event) {
            var cId = $(this).closest('tr').attr('cId');
            var myId = $(this).closest('tr').attr('myId');

            deleteCategoryItem(cId, myId, $(this).closest('tr').remove());
        });

        $('.addCategoryItem').on('click', function (event) {
            event.preventDefault();
            var identifier = $('#inUniqueCategoryItem').val().trim();
            var cid = $(this).closest('tr').attr('cid');
            var addingTr = $(this).closest('tr');
            var type = $('#inTypeCategoryItem').val();
            addCategoryItem(cid, identifier, type,
            function () {
                addingTr.addClass('alert').addClass('alert-success')
                GetCategoryItemsBy(cid, function (data) {
                    $('#categoryItemsContainer tbody').html(renderCategoryItems(data, cid));
                    categoryItemClick();
                });
            }, function () {
                addingTr.addClass('alert').addClass('alert-danger')
            })
        })
    }

    function categoryAddClick() {
        $('.addCategory').click(function () {

            var tr = $(this).closest('tr');
            //tr.removeClass('alert').removeClass('alert-danger').removeClass('alert-success');
            console.log(JSON.stringify({ name: $('#inNameCategory').val(), type: $('#inTypeCategory').val(), isRenderable: $('#inIsRenderableCategory').val() }))
            $.ajax({
                url: '/Categories/AddCategory',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ name: $('#inNameCategory').val(), isRenderable: Number($('#inIsRenderableCategory').val()) }),
                async: true,
                processData: false,
                cache: false,
                success: function (result) {
                    tr.addClass('alert').addClass('alert-success');
                    getCategories(renderCategories);
                },
                error: function (result) {
                    alert('fail')
                    tr.closest('tr').addClass('alert').addClass('alert-danger');
                }
            })
        })
    }

    function categoryEditClick() {
        $('.editCategory').click(function () {

            var tr = $(this).closest('tr');
            id = $('#categoryPopup').attr('categoryid');
            console.log(JSON.stringify({ categoryId: id, name: $('#inNameCategoryEdit').val(), isRenderable: $('#inIsRenderableCategoryEdit').val() }))
            $.ajax({
                url: '/CategoriesEdit/EditCategory',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ categoryId: id, name: $('#inNameCategoryEdit').val(), isRenderable: Number($('#inIsRenderableCategoryEdit').val()) }),
                async: true,
                processData: false,
                cache: false,
                success: function (result) {
                    tr.addClass('alert').addClass('alert-success');
                    $('#categoryPopup').removeClass('is-visible');
                    getCategories(renderCategories);
                },
                error: function (result) {
                    alert('fail')
                    tr.closest('tr').addClass('alert').addClass('alert-danger');
                }
            })
        })
    }
    /*Fakes*/
    var fakeCategoryItems = [
{ identifier: 'facebook.com' }, { identifier: 'facebook.com' }, { identifier: 'facebook.com' }]


    /*Invokes*/

    getCategories(renderCategories);


})