﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices.Model
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RecordAttribute : Attribute
    {
        public RecordAttribute()
        {

        }

        public int MaxLength { get; set; }
        public int Index { get; set; }
        public RecordTypes Type { get; set; }

        public static T Fill<T>(IEnumerable<string> properites) where T : AbstractRecord
        {
            T record = (T)Activator.CreateInstance(typeof(T), new object[] { }); ;
            foreach (var prop in record.GetType().GetProperties())
            {
                if (Attribute.IsDefined(prop, typeof(RecordAttribute)))
                {
                    var attr = Attribute.GetCustomAttribute(prop, typeof(RecordAttribute));
                    if (attr != null)
                    {
                        int index = (int)attr.GetType().GetProperty(nameof(Index)).GetValue(attr);
                        int maxLength = (int)attr.GetType().GetProperty(nameof(MaxLength)).GetValue(attr);
                        RecordTypes type = (RecordTypes)attr.GetType().GetProperty(nameof(Type)).GetValue(attr);

                        if (index >= properites.Count())
                            throw new InvalidRecordProperitesCountException();

                        if (properites.ElementAt(index).Length > maxLength)
                            throw new InvalidRecordFieldExcepction(prop.Name, nameof(MaxLength));

                        try
                        {
                            var obj = Convert(properites.ElementAt(index), type);
                            prop.SetValue(record, obj);
                        }
                        catch (Exception e)
                        {
                            throw new InvalidRecordFieldExcepction(prop.Name, nameof(Type));
                        }

                    }
                }
            }

            return record;
        }

        private static object Convert(string value, RecordTypes type)
        {
            switch (type)
            {
                case RecordTypes.DateTime:
                    return DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                case RecordTypes.String:
                    return value;
                case RecordTypes.Int:
                    return int.Parse(value);
                case RecordTypes.Double:
                    return double.Parse(value);
                case RecordTypes.Bool:
                    return bool.Parse(value);
                default:
                    return value;
            }
        }

    }
}
