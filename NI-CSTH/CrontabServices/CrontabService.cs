﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices
{
    public class CrontabServices
    {
        #region Fields

       

        #endregion
        #region Properties

        #endregion

        #region Methods


        public static void StartPipeServer()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    using (var server = new NamedPipeServerStream("CrontabServices"))
                    {
                        server.WaitForConnection();
                        using (var reader = new StreamReader(server))
                        {
                            var line = reader.ReadLine();
                            if (line != null)
                                CommandLineParser.Parse(line);
                        }
                    }
                }
            });
        }

        public static void NotifyPipeServer(string toNotify)
        {
            using (var client = new NamedPipeClientStream("CrontabServices"))
            {
                client.Connect();
                using (StreamWriter writer = new StreamWriter(client))
                {

                    writer.WriteLine(toNotify);
                    writer.Flush();
                }
            }
        }

        #endregion
    }
}
