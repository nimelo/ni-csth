﻿$(function () {
    function tableClick() {
        $('#sysParametersContainer table tbody tr').click(function () {

            $('.edit').remove();

            if (!$(this).hasClass('edit')) {
                if (!$(this).next().hasClass('edit')) {
                    $(this).after(renderSysParametersTR(rowSysParametersToJson($(this)[0])))
                    me = $(this)

                    $('.saveSysParameters').click(function () {
                        key = me[0].querySelector('td:nth-child(3)').innerHTML
                        value = $('#inSysParameters').val();
                        modifySystemParameter(key, value, function () {
                            me.closest('tr')
                                .removeClass('alert-danger')
                                .addClass('alert-success');

                            setTimeout(getSystemParameters(renderSystemParameters), 2000);
                        }, function () {
                            me.closest('tr')
                                    .addClass('alert-danger')
                        })


                    })
                }
            }
        })
    }
    function rem(el) {
        el.remove();
    }

    function renderSysParametersTR(atrs) {
        var html = "";
        html += '<tr class="edit alert alert-danger">'

        html += '<td>' + atrs.id + '</td>'
        html += '<td>' + atrs.name + '</td>'
        html += '<td>' + atrs.key + '</td>'
        html += '<td>' + '<input id="inSysParameters" type="text" class="form-control" />' + '</td>'
        html += '<td>' + '<button class="saveSysParameters btn glyphicon glyphicon-ok-circle"></button>' + '</td>'

        html += '</tr>'
        return html;
    }

    function rowSysParametersToJson(row) {
        var json = Object();

        json.id = row.querySelector('td:nth-child(1)').innerHTML
        json.name = row.querySelector('td:nth-child(2)').innerHTML
        json.key = row.querySelector('td:nth-child(3)').innerHTML
        json.value = row.querySelector('td:nth-child(4)').innerHTML

        return json;
    }

    function modifySystemParameter(key, value, callback, callbackFail) {
        $.post("/SystemParameters/ModifySystemParameter", { key: key, value: value }, function () {
            callback();
        }).fail(function () {
            callbackFail();
        })
    }

    function getSystemParameters(callback) {
        $.get("/SystemParameters/GetSystemParameters", {}, function (data) {
            callback(data);
        }).fail(function () {
            alert('fail')
        })
    }

    function renderSystemParameters(data) {
        html = "";

        $.each(data, function (index, el) {
            html += '<tr>'
            html += '<td>' + el.Id + '</td>'
            html += '<td>' + el.Name + '</td>'
            html += '<td>' + el.Key + '</td>'
            html += '<td>' + el.Value + '</td>'
            html += '</tr>'
        })

        $('#sysParametersContainer tbody').html(html);
        tableClick();
    }

    getSystemParameters(renderSystemParameters);
})