﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrontabServices
{
    public class Configuration
    {
        public string RootPath { get; set; }
        public string InputFolderName { get; set; }
        public string ErrorFolderName { get; set; }
    }
}
